<?php

ob_start();
session_start();
define('ENV', 'development');

//check if admin flag is set
if(empty($_SESSION['is_admin']))
{
  $_SESSION['flashMsg']['error'] = "Please login to view admin dashboard";
  header('Location: /?p=login');
  die;
}

if(empty($_SESSION['CsrfToken']))
{
  $_SESSION['CsrfToken'] =  md5(uniqid(mt_rand(),true));
}

require __DIR__ . '/../../config/config.php';

$databaseLogger = new DatabaseLogger($dbh);

$file = __DIR__ . '/../../logs/events.log';
$fh = fopen($file, 'a');

$fileLogger = new FileLogger($fh);

$allowed = ['index','clothing','users','construction','orders','clothing_delete',
'clothing_edit','clothing_add','process_clothing_add'];

if(empty($_GET['p'])) {
  $page = 'home';
} elseif(in_array($_GET['p'], $allowed)) {
  $page = $_GET['p'];
} else {
  $page = 'not_found';
  //http_response_code(404);
  header("HTTP/1.1 404 Not Found");
  die();
}
if($page === 'index'){
  $page= 'home';
}

//if database logger returns -1. Failed. Log to file as back up
if(logEvent($databaseLogger) < 0)
{
  logEvent($fileLogger);
}

// //Load flash messages is any
// $flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
// unset($_SESSION['flashMsg']);

$path = __DIR__ . '/../../app/Lib/Controllers/adm_' . $page . '.php';
require($path);