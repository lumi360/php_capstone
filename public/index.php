<?php

ob_start();
session_start();
define('ENV', 'development');//production

//gst 5% PST 7% from sub total

if(empty($_SESSION['CsrfToken']))
{
  $_SESSION['CsrfToken'] =  md5(uniqid(mt_rand(),true));
}

require __DIR__ . '/../config/config.php';

//require __DIR__ . '/../vendor/autoload.php';
//use App\Lib\Interfaces\DatabaseLogger;
//use App\Lib\Interfaces\FileLogger;
$databaseLogger = new DatabaseLogger($dbh);

$file = __DIR__ . '/../logs/events.log';
$fh = fopen($file, 'a');

$fileLogger = new FileLogger($fh); 

$allowed = ['collection', 'contact', 'detail', 'index','faq','how_it_works','profile',
'register','process_register','login','process_login','process_logout','view_cart',
'handle_cart','checkout','process_checkout','thankyou'];

if(empty($_GET['p'])) {
  $page = 'home';
} elseif(in_array($_GET['p'], $allowed)) {
  $page = $_GET['p'];
} else {
  $page = 'not_found';
  //http_response_code(404);
  header("HTTP/1.1 404 Not Found");
}
if($page === 'index'){
  $page= 'home';
}

//if database logger returns -1. Failed. Log to file as back up
if(logEvent($databaseLogger) < 0)
{
  logEvent($fileLogger);
}

$path = __DIR__ . '/../app/Lib/Controllers/' . $page . '.php';
require($path);