<?php

/**
 * Dashboard
 */
class Dashboard extends Model
{
  protected $table = "users";

  public function getDbh()
  {
    return self::$dbh;
  }

  public function getTotalUser_Product_Orders($dbh)
  {
    
    Model::init($dbh);
    $query = "SELECT event FROM log ORDER BY created_at DESC LIMIT 10";
    $query ="SELECT 
    (select count(id) FROM users where is_deleted =0) as user_count,
    (select count(id) from clothing where is_deleted =0) as clothing_count, 
    (select count(id) from orders) as order_count";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }
}