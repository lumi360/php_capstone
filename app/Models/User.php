<?php

class User extends Model
{
  protected $table = "users";

  public function getDbh()
  {
    return self::$dbh;
  }

  public function saveUser($userArray):int
  {
    $paramKey = [];
    $tableNames = "";
    $paramKeyStr = "";
    $paramValues = [];
    foreach($userArray as $key=>$val)
    {
      $paramKey[] = ":$key";
      $paramValues[":$key"] = $val;
    }

    $paramKeyStr = implode(',',$paramKey);
    $tableNames = str_replace(':','',$paramKeyStr);

    $query = "INSERT INTO {$this->table}
              ({$tableNames})
              VALUES 
              ({$paramKeyStr})";
    
    $stmt = self::$dbh->prepare($query);

    foreach($paramValues as $key=>$val)
    {
      $stmt->bindValue("$key",$val);
    }

    $stmt->execute();

    $inserted_id = self::$dbh->lastInsertId();

    return $inserted_id;
  }

  public function deleteUser($userid):bool
  {
    $query = "DELETE FROM {$this->table}
              WHERE id=:userid";
    
    $stmt = self::$dbh->prepare($query);
    $stmt->bindValue(":userid",$userid);

    return $stmt->execute();
    
  }

  public function updateUser($userid,$userData):bool
  {
    $setArray = [];
    $paramValues = [];
    foreach($userData as $key=>$val)
    {
      $setArray[] = "$key=:$key";
      $paramValues[":$key"] = $val;
    }

    $setStr = implode(',',$setArray);
    $query = "UPDATE {$this->table} SET 
            {$setStr} WHERE
            id=:userid";
    
    $stmt = self::$dbh->prepare($query);

    $stmt->bindValue(":userid",$userid);
    foreach($paramValues as $key=>$val)
    {
      $stmt->bindValue("$key",$val);
    }

    return $stmt->execute();

  }

  public function getAllUsers($dbh)
  {
    Model::init($dbh);
    $query = "SELECT * from users where is_deleted = 0 order by created_at desc";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }

}