<?php

class Clothing extends Model
{
  protected $table = "clothing";

  public function getDbh()
  {
    return self::$dbh;
  }

    
  /**
   * getAllDesigners - fetch all distinct designers from db
   *
   * @param  mixed $dbh
   * @return void - return array of designers
   */
  public function getAllDesigners($dbh)
  {
    Model::init($dbh);
    $query = "SELECT distinct designer from clothing where is_deleted= 0 order by designer";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }

   /**
   * getAllFabrics - fetch all distinct fabric from db
   *
   * @param  mixed $dbh
   * @return void - return array of fabrics
   */
  public function getAllFabrics($dbh)
  {
    Model::init($dbh);
    $query = "SELECT distinct fabric from clothing where is_deleted= 0 ";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }

    
  /** 
   * getFullClothingData - fetch all information for clothing where is_deleted = 0
   *
   * @param  mixed $dbh
   * @return void - return array of clothing table info
   */
  public function getFullClothingData($dbh)
  {
    Model::init($dbh);
    $query = "SELECT * from clothing where is_deleted= 0 order by updated_at desc";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }

  public function getFullClothingDataById($dbh,$id)
  {
    Model::init($dbh);
    $query = "SELECT * from clothing where is_deleted= 0 and id=:id  order by created_at desc";
    $paramsArr = [
      ":id"=>$id
    ];
    $data = $this->getDataWithDynamicQuery($query,$paramsArr);
    return $data[0];
  }

  /** 
   * getFullClothingDataByDesigner - fetch all information for clothing  based on designer value
   *
   * @param  mixed $dbh
   * @return void - return array of clothing table info
   */
  public function getFullClothingDataByDesigner($dbh,$designer)
  {
    Model::init($dbh);
    $query = "SELECT * from clothing where designer = :designer and is_deleted= 0 ";
    $paramsArr = [
      ":designer"=>$designer
    ];
    $data = $this->getDataWithDynamicQuery($query,$paramsArr);
    return $data;
  }
  
  /**
   * getFullClothingDataBySearch - fetch all clothing information based on
   * designer/title/colour/fabric
   *
   * @param  mixed $dbh
   * @param  mixed $search
   * @return void
   */
  public function getFullClothingDataBySearch($dbh,$search)
  {
    Model::init($dbh);
    $query = "SELECT * FROM clothing WHERE CONCAT(designer,title,colour,fabric) LIKE :search and is_deleted= 0 ";
    $paramsArr = [
      ":search"=>"%$search%"
    ];
    $data = $this->getDataWithDynamicQuery($query,$paramsArr);
    return $data;
  }
  
  /**
   * getFullClothingDataByDesignerOrFabric - fetch all clothing information based on
   * designer/fabric
   *
   * @param  mixed $dbh
   * @param  mixed $search
   * @return void
   */
  public function getFullClothingDataByDesignerOrFabric($dbh,$search)
  {
    Model::init($dbh);
    $query = "SELECT * FROM clothing WHERE CONCAT(designer,fabric) LIKE :search and is_deleted= 0";
    $paramsArr = [
      ":search"=>"%$search%"
    ];
    $data = $this->getDataWithDynamicQuery($query,$paramsArr);
    return $data;
  }
  
  /**
   * getFullClothingDataBySlugName - fetch all clothing information based on
   * slug column value
   *
   * @param  mixed $dbh
   * @param  mixed $slug
   * @return void
   */
  public function getFullClothingDataBySlugName($dbh,$slug)
  {
    Model::init($dbh);
    $query = "SELECT * from clothing where slug = :slug and is_deleted= 0";
    $paramsArr = [
      ":slug"=>$slug
    ];
    $data = $this->getDataWithDynamicQuery($query,$paramsArr);
    return $data;
  }

    
  /**
   * getMinMaxAvgPriceClothing - get minimum, maximum and average price of all available clothing items
   *
   * @param  mixed $dbh
   * @return void
   */
  public function getMinMaxAvgPriceClothing($dbh)
  {
    Model::init($dbh);
    $query = "SELECT MIN(price) as min_price ,MAX(price) as max_price, 
    round(AVG(price),2) as avg_price from clothing where is_deleted =0";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }
  
  /**
   * deleteClothingItem - Sets clothing item is_deleted flag to 1 to remove item from view
   *
   * @param  mixed $dbh
   * @param  mixed $id
   * @return int
   */
  public function deleteClothingItem($dbh,$id):int
  {
    Model::init($dbh);
    $query = "UPDATE clothing set is_deleted = 1 where id=:id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(":id",$id);
    $stmt->execute();
    return $stmt->rowCount();
  }
  
  /**
   * getQualityEnums - Returns enum values for quality column  in clothing table
   *
   * @param  mixed $dbh
   * @return void
   */
  public function getQualityEnums($dbh)
  {
    Model::init($dbh);
    $query = "SHOW COLUMNS from clothing like 'quality'";
    $stmt = $dbh->prepare($query);
    $stmt->execute();
    $row = $stmt->fetch();
    $enumType  =$row['Type'];
    $enums = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $enumType));
    return $enums;
  }
  
  /**
   * insertNewClothingItem - Collects associate array of clothing data to insert into clothing 
   * table
   *
   * @param  mixed $dbh
   * @param  mixed $itemarray
   * @return void
   */
  public function insertNewClothingItem($dbh,$itemarray)
  {
    Model::init($dbh);
    $data = $this->insert($itemarray);
    return $data;
  }
  
  /**
   * updateClothingItem -  Collects id and associated array of clothing data to update 
   *
   * @param  mixed $dbh
   * @param  mixed $itemarray
   * @return void
   */
  public function updateClothingItem($dbh,$itemarray,$id)
  {
    Model::init($dbh);
    $data = $this->update($id,$itemarray);
    return $data;
  }


}