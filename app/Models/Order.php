<?php

/**
 * Order
 */
class Order extends Model
{
  protected $table = "orders";

  public function getDbh()
  {
    return self::$dbh;
  }

  public function getMinMaxAvgPriceOrders($dbh)
  {
    Model::init($dbh);
    $query = "SELECT MIN(total) as min_total ,MAX(total) as max_total, 
    round(AVG(total),2) as avg_total from orders";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }

  public function getAllOrders($dbh)
  {
    Model::init($dbh);
    $query = "SELECT users.first_name, 
              users.last_name, 
              users.email,
              orders.created_at as order_date,
              orders.sub_total,
              orders.gst+orders.pst as taxes,
              orders.total,
              lineitems.quantity as no_of_items
              FROM 
              orders
               JOIN users on  orders.user_id =users.id 
               JOIN lineitems on orders.id = lineitems.order_id
              order by order_date desc;";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }

  public function getOrderById($dbh,$id)
  {
    $query = "SELECT
        orders.*,
        users.first_name,
        users.last_name,
        users.street,
        users.city,
        users.province,
        users.postal_code,
        users.email,
        clothing.title,
        lineitems.unit_price as price,
        lineitems.quantity as qty,
        lineitems.line_total as line_price

        from
        orders
        JOIN users on orders.user_id = users.id
        JOIN lineitems on orders.id = lineitems.order_id
        JOIN clothing on lineitems.clothing_id = clothing.id
        where
        orders.id = :id";

    $stmt = $this->getDbh()->prepare($query);

    $stmt->bindValue(':id', $id);

    $stmt->execute();

    $result = $stmt->fetchAll();

    return $result;
  }

  public function saveOrder($dbh,$user_id,$cart,$card_num)
  {
    if(!empty($cart))
    {
      $subtotal=0;
      foreach($cart as $line_item){
          $subtotal+=$line_item['line_price'];   
      }
      $pst=0.07*$subtotal;
      $gst=0.05*$subtotal;
      $total=$subtotal+$gst+$pst;
    }

    try
    {
      Model::init($dbh);
      $this->getDbh()->beginTransaction();
      $query = "INSERT INTO {$this->table}
                (  
                  user_id,
                  address, 
                  sub_total, 
                  gst, 
                  pst, 
                  total,
                  card_no,
                  auth_code
                )
                VALUES
                (
                  :user_id,
                  :address,
                  :subtotal, 
                  :gst, 
                  :pst, 
                  :total,
                  :card_no,
                  :auth_code
                )";
      $address = $this->getUserAddress($user_id);

      //preparing quering
      $stmt = $this->getDbh()->prepare($query);
      //binding values to placeholder
      $stmt->bindValue(':subtotal', $subtotal);
      $stmt->bindValue(':pst', $pst);
      $stmt->bindValue(':gst', $gst);
      $stmt->bindValue(':total', $total);
      $stmt->bindValue(':auth_code', uniqid());
      $stmt->bindValue(':card_no', substr($card_num,12));
      $stmt->bindValue(':address', json_encode($address));
      $stmt->bindValue(':user_id', $user_id);

      $stmt->execute();

      $order_id = $this->getDbh()->lastInsertId();
      $this->saveLineItems($cart, $order_id);

      $this->getDbh()->commit();

      return $order_id;
    }
    catch(\PDOException $e){
      $this->getDbh()->rollBack();
      echo $e->getMessage();
      die;
    }
  }

  public function saveLineItems($cart,$order_id)
  {
    //clothing_id | order_id | unit_price | quantity | line_total | created_at          | updated_at 
    $query = "INSERT INTO lineitems
            (  
              clothing_id,
              order_id,
              unit_price, 
              quantity, 
              line_total
            )
              VALUES
            (
              :clothing_id,
              :order_id,
              :unit_price, 
              :quantity,
              :line_total
            )";

    $stmt = $this->getDbh()->prepare($query);
    foreach($cart as $item)
    {
      //binding values to placeholder
      $stmt->bindValue(':clothing_id',$item['id'] );
      $stmt->bindValue(':order_id', $order_id);
      $stmt->bindValue(':unit_price', $item['price']);
      $stmt->bindValue(':quantity', $item['qty']);
      $stmt->bindValue(':line_total', $item['line_price']);

      $stmt->execute();
    }


  }

  public function getUserAddress($user_id)
  {
    $query = "SELECT 
              *
              FROM users
              WHERE
              id = :id";

    $stmt = $this->getDbh()->prepare($query);
    $stmt->bindValue(':id', $user_id);
    $stmt->execute();

    $result = $stmt->fetch();
    return $result;
  }


}