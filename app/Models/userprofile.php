<?php


/**
 * getUserByID
 *
 * @return array
 */
function getUserByID($id):array
{
  global $dbh;
  //script to get user info
  $query = "SELECT * FROM 
            users 
            where 
            id=:id";

  $stmt = $dbh->prepare($query);

  $stmt->bindValue(':id',$id);

  $stmt->execute();

  $user_profile = $stmt->fetch();
  return !$user_profile ? [] : $user_profile;
}


/**
 * insertUserData 
 *
 * @return int last inserted table id
 */
function insertUserData($hashed_password):int
{
  global $dbh;

  try
  {
    //query to insert user info after successful validation
    $query = "INSERT INTO users
    (
      first_name,
      last_name,
      street,
      city,
      postal_code,
      province,
      country,
      phone,
      email,
      password,
      subscribed_to_newsletter
    )
    VALUES 
    (
      :first_name,
      :last_name,
      :street,
      :city,
      :postal_code,
      :province,
      :country,
      :phone,
      :email,
      :password,
      :subscribed_to_newsletter
    )";

    $stmt = $dbh->prepare($query);

    $stmt->bindValue(':first_name', ucwords($_POST['first_name']));
    $stmt->bindValue(':last_name', ucwords($_POST['last_name']));
    $stmt->bindValue(':street',ucwords($_POST['street']));
    $stmt->bindValue(':city',ucwords($_POST['city']));
    $stmt->bindValue(':postal_code', str_replace(' ', '',$_POST['postal_code']));//remove spaces before submitting to db
    $stmt->bindValue(':province', ucwords($_POST['province']));
    $stmt->bindValue(':country', ucwords( $_POST['country']));
    $stmt->bindValue(':phone', $_POST['phone']);
    $stmt->bindValue(':email', strtolower($_POST['email']));
    $stmt->bindValue(':password', $hashed_password);
    $stmt->bindValue(':subscribed_to_newsletter', $_POST['subscribe'] ?? 0);

    $stmt->execute();

    $id = $dbh->lastInsertId();

    return $id;
  }
  catch(Exception $e)
  {
    dc($e->getMessage());
    return -1;
  }
  
}


/**
 * checkUserEmailIsUnique - Calls db to ensure email does not exist
 *
 * @param  mixed $email
 * @return bool - true means unique, false means email already taken
 */
function checkUserEmailIsUnique(string $email):bool
{
  global $dbh;
  
  $query="SELECT id FROM
          users 
          WHERE email=:email";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':email', $email);
  $stmt->execute();

  $user_profile = $stmt->fetch();
  return !empty($user_profile) ? false : true;

}

/**
 * validateUserLoginCredentials - Validates user email with password in db
 * Also assigns user id to session
 *
 * @param  mixed $email
 * @param  mixed $password
 * @return bool
 */
function validateUserLoginCredentials(string $email, string $password):bool
{
  global $dbh;
  
  $query="SELECT * FROM
          users 
          WHERE email=:email";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':email', $email);
  $stmt->execute();

  $user_profile = $stmt->fetch();

  if(empty($user_profile) || !password_verify($password,$user_profile['password']))
  {
    return false;
  }
  else{
    
    //assign user id to session
    $_SESSION['profile_id'] = $user_profile['id'];
    return true;
  }
}

function checkIfUserIsAdmin(string $email, $dbh):bool
{
  $query="SELECT is_admin FROM
          users 
          WHERE email=:email and is_admin = 1";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':email', $email);
  $stmt->execute();

  $user_profile = $stmt->fetch();

  if(empty($user_profile) )
  {
    return false;
  }
  else{
    return true;
  }
}