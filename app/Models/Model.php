<?php

abstract class Model
{

    protected static $dbh;

    protected $table;

    protected $key = 'id';

    public static function init($dbh)
    {
      self::$dbh = $dbh;
    }

    public function getDbh()
    {
      return self::$dbh;
    }

    // get all records from a table
    public function getAll()
    {

      $query = "SELECT * FROM {$this->table}";

      $stmt = self::$dbh->prepare($query);

      $stmt->execute();

      return $stmt->fetchAll();

    }

    // get one record from a table
    public function getOne($id)
    {

      $query = "SELECT * FROM {$this->table}
                  WHERE {$this->key} = :id";

      $stmt = self::$dbh->prepare($query);

      $stmt->bindValue(':id', $id);

      $stmt->execute();

      return $stmt->fetch();

    }

        
    /**
     * insert - collect array of data to insert into table
     *
     * @param  mixed $arr
     * @return int
     */
    public function insert($arr):int
    {
      $paramKey = [];
      $tableNames = "";
      $paramKeyStr = "";
      $paramValues = [];
      try
      {
        foreach($arr as $key=>$val)
        {
          $paramKey[] = ":$key";
          $paramValues[":$key"] = $val;
        }

        $paramKeyStr = implode(',',$paramKey);
        $tableNames = str_replace(':','',$paramKeyStr);

        $query = "INSERT INTO {$this->table}
                  ({$tableNames})
                  VALUES 
                  ({$paramKeyStr})";
        
        $stmt = self::$dbh->prepare($query);

        foreach($paramValues as $key=>$val)
        {
          $stmt->bindValue("$key",$val);
        }

        $stmt->execute();

        $inserted_id = self::$dbh->lastInsertId();

        return $inserted_id;
      }
      catch(Exception $e)
      {
        return -1;
      }
      
    }
    
    /**
     * update - collects array of data to update based on table primary id
     *
     * @param  mixed $id
     * @param  mixed $Data
     * @return bool
     */
    public function update($id,$Data):bool
    {
      $setArray = [];
      $paramValues = [];
      foreach($Data as $key=>$val)
      {
        $setArray[] = "$key=:$key";
        $paramValues[":$key"] = $val;
      }

      $setStr = implode(',',$setArray);
      $query = "UPDATE {$this->table} SET 
              {$setStr} WHERE
              id=:id";
      
      $stmt = self::$dbh->prepare($query);

      $stmt->bindValue(":id",$id);
      foreach($paramValues as $key=>$val)
      {
        $stmt->bindValue("$key",$val);
      }

      return $stmt->execute();

    }

    public function getDataWithDynamicQuery($query, $paramsArr = [])
    {
      $stmt = self::$dbh->prepare($query);

      foreach($paramsArr as $key=>$val)
      {
        // $stmt->bindValue("$key",$val);
        $stmt->bindValue("$key",$val);
      }
      $stmt->execute();

      return $stmt->fetchAll();
    }
    // abstract public function save($array);

    // abstract public function update($array);

    // abstract public function delete($id);


}