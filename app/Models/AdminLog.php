<?php

class AdminLog extends Model
{
  protected $table = "log";

  public function getDbh()
  {
    return self::$dbh;
  }

  public function loadLast10DbLogEntries($dbh)
  {
    Model::init($dbh);
    $query = "SELECT event FROM log ORDER BY created_at DESC LIMIT 10";
    $data = $this->getDataWithDynamicQuery($query);
    return $data;
  }
}