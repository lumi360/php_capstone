<?php include __DIR__ . '/includes/adm_header.inc.php';?>
<div class="row">
  <div class="col-12">
    <?php include __DIR__ . '/includes/flash.message.inc.php';?>

    <h1>Dashboard</h1>
    <br/>
    <h2>Some Analytics..</h2>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">Overview</th>
          <th scope="col">Clothing</th>
          <th scope="col">Orders</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <?php foreach($dashboardOverview as $row) : ?>
            <td>
              <strong>Total Users: </strong><?=esc($row['user_count'])?><br/>
              <strong>Total Clothing: </strong><?=esc($row['clothing_count'])?><br/>
              <strong>Total Orders: </strong><?=esc($row['order_count'])?><br/>
            </td>
          <?php endforeach; ?> 
          <?php foreach($clothingOverview as $row) : ?>
            <td>
              <strong>Max Price: </strong>$<?=esc(number_format($row['max_price'] ?? 0,2))?><br/>
              <strong>Min Price: </strong>$<?=esc(number_format($row['min_price'] ?? 0,2))?><br/>
              <strong>Average Price: </strong>$<?=esc(number_format($row['avg_price'] ?? 0,2))?><br/>
            </td>
          <?php endforeach; ?>
          <?php foreach($orderOverview as $row) : ?>
            <td>
              <strong>Max Order: </strong>$<?=esc(number_format($row['max_total'] ?? 0,2))?><br/>
              <strong>Min Order: </strong>$<?=esc(number_format($row['min_total'] ?? 0,2))?><br/>
              <strong>Average Order: </strong>$<?=esc(number_format($row['avg_total'] ?? 0,2))?><br/>
            </td>
          <?php endforeach; ?> 
        </tr>

      </tbody>
    </table>
    <br/>
    <h2>Recent Log Entries</h2>
    
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Log Entries (Latest first) > DateTime | RequestMethod | RequestPath | HTTPStatusCode | UserAgent</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($filteredLogData as $LogData) : ?>
          <?php foreach($LogData as $key => $value) : ?>
            <?php if (!str_contains($value,"| 200 |")) :?>
              <tr><td class="table-warning"><?=esc($value)?></td></tr>
            <?php else : ?>
              <tr><td class="table-success"><?=esc($value)?></td></tr>
            <?php endif ?>
          <?php endforeach; ?>   
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>     
</div>
<?php include __DIR__ . '/includes/adm_footer.inc.php';?>
