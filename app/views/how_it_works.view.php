<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center" style="padding-bottom: 60px"><?=esc($title)?></h1>

<div id="about_us_pic_div" class="floatleft">
  <img src="images/about_us_pic.jpg" alt="Wedding Pic" width="400" height="512">
</div>
<h2 class="justified_text">How to list outfits</h2>
<p class="justified_text">
  SavingEverAfter is a wedding themed peer to peer outfit rental company. All outfits and 
  accessories available to rent are taken from former brides, grooms, bridesmaids, and 
  groomsmen and provided at highly subsidized fees to current wedding party members who need them.  
  We believe your wedding outfits are an investment and you can earn back money from it! 
  We provide affordable prices, flexible length or rental (4, 7 or 14 days), delivery option 
  (pickup or delivery) and even allow fittings. As the Sharer, all you need to do is list your 
  outfit with us and we contact you when a rental or sale option is available – simple.
  All you have to do is enlist your outfit  
  <a href="https://bit.ly/SEAListing" title="List your outfits here" style="text-decoration: none; font-weight: 700; color: #000">HERE</a> 
  - this includes details 
  of your outfit, images and payment details. Your listing is then prepared to be shared after review 
  (we check image quality, accuracy of description and more; if we need more information, we’ll get 
  in touch)
</p>

<h2 class="clearfloat">Photo Requirements:</h2>
<p class="justified_text">
  We would really love your outfit to be well represented and for our renters to see the item at its 
  best so please help share the best photos. If you would not like your face shown, you can crop the 
  image to hide your identity.
</p>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>