<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center" style="padding-bottom: 60px"><?=esc($title)?></h1>

<!-- Women Clothing size table -->
<table class="my_table">
  <caption>Clothing Size Chart (inches)</caption><!-- Caption for table -->
  <tr>
    <th colspan="4">Women's Sizing</th> <!-- Main HEADER for table -->
  </tr>

  <tr><!-- Start of Sub-Headers Row -->
    <th>General Size</th>
    <th>Chest/Bust Measurement</th>
    <th>Waist Measurement</th>
    <th>Hips</th>
  </tr><!-- End of Sub-Headers Row -->

  <tr><!-- Start of First Row -->
    <td>XS</td>
    <td>32"-33"</td>
    <td>24"-25"</td>
    <td>34"-35.5"</td>
  </tr><!-- End of First Row -->
  
  <tr><!-- Start of Second Row -->
    <td>S</td>
    <td>34"-35"</td>
    <td>26"-27"</td>
    <td>36"-37"</td>
  </tr><!-- End of Second Row -->
  
  <tr><!-- Start of Third Row -->
    <td>M</td>
    <td>36"-37"</td>
    <td>28"-29"</td>
    <td>38"-39.5"</td>
  </tr><!-- End of Third Row -->
  
  <tr><!-- Start of Fourth Row -->
    <td>L</td>
    <td>38"-39"</td>
    <td>30"-32"</td>
    <td>40"-42.5"</td>
  </tr><!-- End of Fourth Row -->
</table>
<!-- End of women clothing size table -->

<!-- MEN Clothing size table -->
<table class="my_table">
  <caption>Clothing Size Chart (inches)</caption><!-- Caption for table -->
  <tr>
    <th colspan="4">Men's Sizing</th> <!-- Main HEADER for table -->
  </tr>

  <tr><!-- Start of Sub-Headers Row -->
    <th>General Size</th>
    <th>Neck</th>
    <th>Sleeve Length</th>
    <th>Chest</th>
  </tr><!-- End of Sub-Headers Row -->

  <tr><!-- Start of First Row -->
    <td>S</td>
    <td>14.5"-15"</td>
    <td>33"-34"</td>
    <td>36"-37"</td>
  </tr><!-- End of First Row -->
  
  <tr><!-- Start of Second Row -->
    <td>M</td>
    <td>15.5"-16"</td>
    <td>35"-36"</td>
    <td>38"-40"</td>
  </tr><!-- End of Second Row -->
  
  <tr><!-- Start of Third Row -->
    <td>L</td>
    <td>16.5"-17"</td>
    <td>37"-38"</td>
    <td>41"-43"</td>
  </tr><!-- End of Third Row -->
  
  <tr><!-- Start of Fourth Row -->
    <td>XL</td>
    <td>17.5"-18"</td>
    <td>39"-40"</td>
    <td>44"-46"</td>
  </tr><!-- End of Fourth Row -->
</table>
<!-- End of Men clothing size table -->

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>