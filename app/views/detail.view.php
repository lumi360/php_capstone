<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center"><?=esc($title)?></h1>

<?php include __DIR__ . '/includes/flash.message.inc.php';?>

<?php if(!empty($detailResultMsg)) : ?>
  <h2><strong><?=esc($detailResultMsg)?></strong></h2>
<?php endif; ?>

<p><label for="category">Filter by Category: </label>
  <select id="category" onchange="CategoryFilterFunc(event, this)">
            <option value="">Pick a Designer</option>
            <optgroup label="Designers">
              <?php foreach($designers as $Data) : ?>   
                <?php foreach($Data as $key => $value) : ?>   
                  <option value="<?=esc(str_replace(' ','-',$value))?>"><?=esc(format_Label($value))?></option>
                <?php endforeach; ?>   
              <?php endforeach; ?>   
            </optgroup>
  </select>
</p>

<?php foreach($detailView as $row) : ?>
  <div> 
    <img class="floatleft" src="images/<?=esc($row['image_name'])?>" alt="<?=esc($row['title'])?>" width="400" height="326">
    <p class="justified_text"><strong><?=esc(format_Label($row['title']))?></strong> - <?=esc(format_Label($row['summary']))?></p>
    <p class="justified_text"><strong>Price: </strong>$<?=esc($row['price'])?></p>
    <p class="justified_text"><strong>Designer: </strong><?=esc(format_Label($row['designer']))?></p>
    <p class="justified_text"><strong>Condition: </strong><?=esc($row['quality'])?></p>
    <p class="justified_text"><strong>Fabric: </strong><?=esc(format_Label($row['fabric']))?></p>
    <p class="justified_text"><strong>Colour: </strong><?=esc(format_Label($row['colour']))?></p>
    <p class="justified_text"><strong>Size: </strong><?=esc($row['size'])?></p>
    <p class="justified_text"><strong>Stock num: </strong><?=esc($row['stock_count'])?></p>
    <p class="justified_text"><strong>Long Description: </strong></p><?=html($row['long_desc'])?>
    <form action="/?p=handle_cart" method="post">
      <p>Quantity: <select name="qty">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
      </select></p>
      <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
      <input type="hidden" name="clothing_id" value="<?=esc($row['id'])?>" />
      <input type="hidden" name="clothing_name" value="<?=esc(str_replace(' ','-',$row['title']))?>" />
      <input type="submit" value="Add to Cart" />
    </form>

  </div>
  <div class="clearfloat"></div>
<?php endforeach; ?> 

<script>
  function CategoryFilterFunc(e,el) {
    var cat = el.value;
    window.location = '/?p=collection&category=' + cat;
  }
</script>
<?php  include __DIR__ . '/includes/footer.inc.php'; ?>