<?php include __DIR__ . '/includes/header.inc.php';?>

<div id="call_to_action_div"><!-- Start of call to action div -->
  <div class="padded_width">
    <h1 id="motto_header" class="position_center"><?=esc($title)?></h1>
    <h2 id="call_to_action" class="position_center">Check Out Our Collection To Rent From <a href="collection.php" title="Check out our Collection" class="def_anchor">HERE</a></h2>
  </div>
  
  <!--<img src="images/Beach_wedding_pic_edit.jpg" alt="Beach_Pic" width="960" height="641">--> 
</div><!-- end of call to action div -->

<div id="register_action"><!-- start of register action div -->
  <div id="left_diamond" class="flex_three">
    <img src="images/Diamond_favicon.svg" alt="diamond_image" width="50" height="50" >
  </div>
  <p class="bold_font flex_three">Earn money. List your outfits <a href="contact.php" class="def_anchor" title="Drop a message">HERE</a></p>
  <div id="right_diamond" class="flex_three">
    <img src="images/Diamond_favicon.svg" alt="diamond_image" width="50" height="50" >
  </div>
</div><!-- end of register action div -->

<div id="vision_statement_div">
  <img src="images/SEA_Vision_statement_600.jpg" alt="vision statement" width="600" height="600">
</div>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>