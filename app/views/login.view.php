<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 style="text-align: center"><?=esc($title)?></h1>

<?php include __DIR__ . '/includes/flash.message.inc.php';?>

<form
  name="login_form"
  id="login_form"
  action="?p=process_login"
  method="post"
  autocomplete = "on"
  novalidate>

  <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
  <fieldset>
    <legend>Login</legend>
    <p><span class="req_icon">*</span><em> indicates a required field.</em></p>

    <p>
    <label for="email"><span class="req_icon">*</span>Email: </label>
    <input type="text"
            id="email"
            name="email"
            placeholder="Please enter your email"
            value=""/>
    <span class="error"><em><?=esc($errors['email'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="password"><span class="req_icon">*</span>Password: </label>
    <input type="password"
            id="password"
            name="password"
            placeholder="Please enter your password" />
    <span class="error"><em><?=esc($errors['password'][0] ?? '')?></em></span>
    </p>
    
    <p><input type="submit" value="Login" /></p>
      
  </fieldset><!-- end of register fieldset -->

</form> <!-- end of register form -->

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>