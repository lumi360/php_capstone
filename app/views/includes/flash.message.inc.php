<?php if(!empty($flashMsg['success'])) : ?>
    <div class="msg_box_good">
        <?=esc($flashMsg['success'])?>
    </div>
<?php endif; ?>

<?php if(!empty($flashMsg['error'])) : ?>
    <div class="msg_box">
        <?=esc($flashMsg['error'])?>
    </div>
<?php endif; ?>