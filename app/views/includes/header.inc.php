<!doctype html>

<html lang="en">
<head>
  <title> <?=esc($title);?></title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
  <!-- CSS Media Queries -->
  <link rel="stylesheet" type="text/css" media="screen and (min-width: 768px)" href="styles/project_desktop.css">
  <link rel="stylesheet" type="text/css" media="screen and (max-width: 767px)" href="styles/project_mobile.css">
  <link rel="stylesheet" type="text/css" media="print" href="styles/project_print.css">
  
  <!-- Favourit Icons list -->
  <link rel="icon" href="images/favicon.png" type="image/x-icon" />
  <link rel="apple-touch-icon" sizes="196x196" href="images/Diamond_favicon_196.png" />
  <link rel="apple-touch-icon" sizes="192x192" href="images/Diamond_favicon_192.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="images/Diamond_favicon_180.png" />
  <link rel="apple-touch-icon" sizes="167x167" href="images/Diamond_favicon_167.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="images/Diamond_favicon_152.png" />
  <link rel="apple-touch-icon" sizes="128x128" href="images/Diamond_favicon_128.png" /> 
   
  <!-- Detect IE 8 or lower -->
    <!--[if LTE IE 8]>
      <h1>Please update your browser to get optimum performance
      </h1>
    <![endif]-->  
  <style>
    
    /* Dynamic css sections based on page index */
    <?php if($page_index == 'Marks' || $page_index == 'FAQ') : ?>
        #home_content{
          padding: 0 15px;
          margin: 20px auto;
          text-align: center;
          min-height: 100vh;
          clear:both;
        }
    <?php elseif($page_index == 'Contact')  : ?>
        #home_content{
          padding: 10px 15px;
          margin: 0;
          clear:both;
          min-height: 100vh;
        }
    <?php elseif($page_index == 'Home') : ?>
        #home_content{
          padding: 0;
          margin: 0;
          min-height: 100vh;
          clear:both;

        }
    <?php elseif($page_index == 'Register') : ?>
        
        #home_content{
          padding: 0;
          margin: 0 15px;
          clear:both;
          min-height: 100vh;
        }
        .req_icon{
          color: #f00;
        }

         
    <?php else : ?>
        #home_content{
          padding: 0;
          margin: 0 15px;
          clear:both;
          min-height: 100vh;
        }
        .req_icon{
          color: #f00;
        }
    <?php endif; ?>
    
    
    <?php if($page_index == 'Marks') : ?>
        a{
          text-decoration: none;
          color: #000;
        }
    <?php endif; ?>
  </style>
</head>

<body>
  
  <div id="wrapper"><!-- Start of Main content wrapper for page -->
   
    <header><!-- start of header tag -->
      <div class="util">
        <ul>
          <li><a href="?p=view_cart" title="SavingEverAfter - Shopping Cart">Cart <span>
            <?php if(!empty($_SESSION['Cart'])) : ?>
              (<?=esc(count($_SESSION['Cart']))?>)
            <?php else : ?>
              (0)
            <?php endif; ?>
          </span></a>
          </li>

          <?php if(!empty($_SESSION['profile_id'])) : ?>
          <!-- utility nav if logged in -->
          <li><a href="?p=profile" title="SavingEverAfter - Profile">Profile</a></li>
          <li>
            <form action="?p=process_logout" id="util_logout" method="post">
              <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
              <input type="submit" name="logout" value="logout" />
            </form>
          </li>
         <?php else : ?>
          <!-- utility nav if not logged in -->
          <li><a href="?p=register" title="Register with us">Register</a></li>
          <li><a href="?p=login" title="Login">Login</a></li>
          <?php endif; ?>
        </ul> 
      </div>
      <div class="header">
        <div id="logo">
          <a href="?p=index" title="HomePage">
            <img src="images/new_SEA_logo_edit.svg" alt="SavingEverAfter Logo" width="200" height="60" />
          </a>
        </div>
        
        <nav><!-- Start of navigation menu -->
          <a href="#" id="hamburger_button">
            <span id="topbar"></span>
            <span id="middlebar"></span>
            <span id="bottombar"></span>
          </a>

          <ul id="navlist">
            <li><a href="?p=index" title="SavingEverAfter - Homepage">Home</a></li>
            <li><a href="?p=collection" title="Our Collection">Collection</a></li>
            <li><a href="?p=how_it_works" title="How It Works">How It Works</a></li>
            <li><a href="?p=contact" title="Reach out to us">Contact</a></li>
            <li><a href="?p=faq" title="Get Answers Here">FAQ</a></li> 
          </ul>
        </nav><!-- end of navigation menu -->
      </div>
      
    </header><!-- end of header tag -->
    
    <!-- Start of main content -->
    <main id="home_content">