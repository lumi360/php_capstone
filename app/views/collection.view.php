<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center"><?=esc($title)?></h1>
<?php include __DIR__ . '/includes/flash.message.inc.php';?>

<div class="floatright">
  <form action="/">
      <input type="hidden" name="p" value="collection" />
      <input type="text" name="search" placeholder="search input" /> 
      <input type="submit" />
  </form>
</div>
<div style="clear:both">

  <?php if(!empty($collectionResultMsg)) : ?>
    <h2><strong><?=esc($collectionResultMsg)?></strong></h2>
  <?php endif; ?>
  <p><label for="category">Filter by Category: </label>
    <select id="category" onchange="CategoryFilterFunc(event, this)">
              <option value="">Pick a Designer</option>
              <optgroup label="Designers">
                <?php foreach($designers as $Data) : ?>   
                  <?php foreach($Data as $key => $value) : ?>   
                    <option value="<?=esc(str_replace(' ','-',$value))?>"><?=esc(format_Label($value))?></option>
                  <?php endforeach; ?>   
                <?php endforeach; ?>   
              </optgroup>
    </select>
  </p>
  <div id="clothing_div">
    <div class= "clothing_row">
      <?php foreach($fullClothingList as $row) : ?>
        <div class="clothing_col"> 
              <a href="?p=detail&name=<?=esc($row['slug'])?>"><img src="images/<?=esc($row['image_name'])?>" 
              alt="<?=esc($row['title'])?>" width="400" height="326"></a>
              <p class="justified_text"><strong><?=esc(format_Label($row['title']))?></strong> 
              - <?=esc(format_Label($row['summary']))?></p>
              <p class="justified_text"><strong>Price: </strong>$<?=esc(number_format($row['price'], 2))?></p>
              <p class="justified_text"><strong>Designer: </strong><?=esc(format_Label($row['designer']))?></p>
              <p class="justified_text"><strong>Condition: </strong><?=esc($row['quality'])?></p>
              <p class="justified_text"><strong>Stock num: </strong><?=esc($row['stock_count'])?></p>
        </div> 
      <?php endforeach; ?>   
    </div>

  </div>

  <script>
    function CategoryFilterFunc(e,el) {
      var cat = el.value;
      window.location = '/?p=collection&category=' + cat;
    }
  </script>

  
  <div class="clearfloat"></div>
  
</div>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>