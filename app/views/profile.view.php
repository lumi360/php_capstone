<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 style="text-align: center"><?=esc($title)?></h1>

<?php include __DIR__ . '/includes/flash.message.inc.php';?>

<!-- if no profile_result message exists show data -->
<?php if(empty($profile_result)) : ?>
  <div class="profile_view">
    <h2>Thank you for registering!! <?=esc(format_Label($user_profile['first_name']))?></h2>
    <h3>Your Information:</h3>
    <p><strong>First Name</strong> : <?=esc(format_Label($user_profile['first_name']))?></p>
    <p><strong>Last Name</strong> : <?=esc(format_Label($user_profile['last_name']))?></p>
    <p><strong>Email</strong> : <?=esc(format_Label($user_profile['email']))?></p>
    <p><strong>Phone No</strong> : <?=esc(format_Label($user_profile['phone']))?></p>
    <p><strong>Street</strong> : <?=esc(format_Label($user_profile['street']))?></p>
    <p><strong>City</strong> : <?=esc(format_Label($user_profile['city']))?></p>
    <p><strong>Postal Code</strong> : <?=esc(format_Label($user_profile['postal_code']))?></p>
    <p><strong>Province</strong> : <?=esc(format_Label($user_profile['province']))?></p>
    <p><strong>Country</strong> : <?=esc(format_Label($user_profile['country']))?></p>
    <br/>
    <p><strong>Newsletter Subscription</strong> : <?=esc(format_Label($user_profile['subscribed_to_newsletter'] ? 'on': 'off'))?></p>
    <?php
    $dt = new Datetime($user_profile['created_at']);
    $createdDate = date_format($dt,'D d-F-Y H:i:s A');
    ?>
    <p><strong>Date Created</strong> : <?=esc($createdDate)?></p>
    
  </div>
<?php endif; ?>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>