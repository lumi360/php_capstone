<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center" style="padding-bottom: 60px"><?=esc($title)?></h1>

<h2 style="margin-top: 20px">Please fill out the form below. If you are interested</h2>
<form action="http://scott-media.com/test/form_display.php"
  method="post"
  id="contact_form"
  autocomplete="on"
  >
  <input type="hidden" name="csrf_token" value="<?=getCsrfToken()?>" />
  <fieldset>
    <legend class="sm_border_radius">Personal Info</legend>
    <p>
      <label for="first_name">First Name: </label>
      <input type="text" 
              name="first_name" 
              id="first_name" 
              maxlength="50"
              size="25"
              placeholder="Please enter your first name"
              required />
    </p>

    <p>
      <label for="last_name">Last Name: </label>
      <input type="text"
              id="last_name"
              name="last_name"
              maxlength="50"
              size="25" 
              placeholder="Please enter your last name"
              required="required" />
    </p>
    
    <p>
      <label for="phone">Phone Number:</label>
      <input type="tel"
              name="phone"
              id="phone"
              placeholder="Please enter your mobile xxx-xxx-xxxx" />
    </p>

    <p>
      <label for="email_address">Email:</label>
      <input type="email"
              name="email_address"
              id="email_address"
              size="25" 
              placeholder="example: abc@gmail.com" />
    </p>
    
  </fieldset>
  
  <fieldset>
    <legend class="sm_border_radius">Other Information</legend>
    
    <p>Please select an appropriate Age Group: <br />
  
      <input type="radio" id="age0_15" name="age_range" value="0-15" />
      <label for="age0_15">0-15 Years </label><br />

      <input type="radio" id="age16_25" name="age_range" value="16-25" />
      <label for="age16_25">16-25 Years</label><br />

      <input type="radio" id="age26_35" name="age_range" value="26-35" />
      <label for="age26_35">26-35 Years</label><br />

      <input type="radio" id="age36_55" name="age_range" value="36-55" />
      <label for="age36_55">36-55 Years</label><br />

      <input type="radio" id="age56_plus" name="age_range" value="56-plus" />
      <label for="age56_plus">55 Years or older</label><br />
    </p>
    
    <p>Choose the group that apply to you:<br />
      
      <input type="checkbox" 
              id="biz_group1" 
              name="biz_group1" 
              value="Lender" />
      <label for="biz_group1">Lender</label><br />
      
      <input type="checkbox" 
              id="biz_group2" 
              name="biz_group2" 
              value="Renter" />
      <label for="biz_group2">Renter</label><br />
    </p>  
    
    <p>
      <label for="comments" class="longlabel">Type Any Additional Comments</label>
      <textarea id="comments"
                name="comments"
                cols="35"
                rows="5"
                placeholder="Type your comments here"></textarea>
    </p> 
    
  </fieldset> 
      
  <p>
    <input type="checkbox"
            id="subscribe"
            name="subscribe"
            value="Yes"
    />
    <label for="subscribe">Subscribe to our weekly newsletter</label>
  </p>
  
  <p>
    <input type="submit" value="Send Form" /> &nbsp; 
    <input type="reset" value="Clear Form" /> &nbsp;
  </p>   
</form>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>