<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 style="text-align: center"><?=esc($title)?></h1>
<p style="text-align: center">Sign up with us today and start earning money on your outfits!</p>

<?php include __DIR__ . '/includes/flash.message.inc.php';?>

<form
  name="register_form"
  id="register_form"
  action="?p=process_register"
  method="post"
  autocomplete = "on"
  novalidate>

  <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
  <fieldset>
    <legend>Register</legend>
    <p><span class="req_icon">*</span><em> indicates a required field.</em></p>

    <p>
    <label for="first_name"><span class="req_icon">*</span>First Name: </label>
    <input type="text" 
            name="first_name" 
            id="first_name"
            placeholder="Please enter your first name"
            value="<?=esc($register_form_values_arr['first_name'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['first_name'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="last_name"><span class="req_icon">*</span>Last Name: </label>
    <input type="text"
            id="last_name"
            name="last_name"
            placeholder="Please enter your last name"
            value="<?=esc($register_form_values_arr['last_name'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['last_name'][0] ?? '')?></em></span>
    </p>


    <p>
    <label for="phone"><span class="req_icon">*</span>Phone: </label>
    <input type="text"
            id="phone"
            name="phone"
            placeholder="Please enter your phone number"
            title="Please enter your phone number"
            value="<?=esc($register_form_values_arr['phone'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['phone'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="email"><span class="req_icon">*</span>Email: </label>
    <input type="text"
            id="email"
            name="email"
            placeholder="Please enter your email"
            value="<?=esc($register_form_values_arr['email'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['email'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="street"><span class="req_icon">*</span>Street: </label>
    <input type="text"
            id="street"
            name="street"
            placeholder="Please enter your street address"
            title= "Please enter your street address"
            value="<?=esc($register_form_values_arr['street'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['street'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="city"><span class="req_icon">*</span>City: </label>
    <input type="text"
            id="city"
            name="city"
            placeholder="Please enter your city"
            value="<?=esc($register_form_values_arr['city'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['city'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="province"><span class="req_icon">*</span>Province: </label>
    <input type="text"
            id="province"
            name="province"
            placeholder="Please enter your province"
            value="<?=esc($register_form_values_arr['province'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['province'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="country"><span class="req_icon">*</span>Country: </label>
    <input type="text"
            id="country"
            name="country"
            placeholder="Please enter your country"
            value="<?=esc($register_form_values_arr['country'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['country'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="postal_code"><span class="req_icon">*</span>Postal Code: </label>
    <input type="text"
            id="postal_code"
            name="postal_code"
            placeholder="Please enter your postal code" 
            value="<?=esc($register_form_values_arr['postal_code'] ?? '')?>"/>
    <span class="error"><em><?=esc($errors['postal_code'][0] ?? '')?></em></span>
    </p>
    <br/>

    <p>
    <label for="password"><span class="req_icon">*</span>Password: </label>
    <input type="password"
            id="password"
            name="password"
            placeholder="Please enter your password" />
    <span class="error"><em><?=esc($errors['password'][0] ?? '')?></em></span>
    </p>

    <p>
    <label for="confirm_password"><span class="req_icon">*</span>Confirm Password: </label>
    <input type="password"
            id="confirm_password"
            name="confirm_password"
            placeholder="Please confirm your password" 
            title="Please confirm your password"/>
    <span class="error"><em><?=esc($errors['confirm_password'][0] ?? '')?></em></span>
    </p>
    
    <p>
    <input type="checkbox"
            id="subscribe"
            name="subscribe"
            value="1"
            <?php echo (!empty($register_form_values_arr['subscribe']) ? 'checked' : '');?>

    />
    <label for="subscribe">Subscribe to our newsletter</label>
    </p>

    <p><input type="submit" value="Register" /></p>
      
  </fieldset><!-- end of register fieldset -->

</form> <!-- end of register form -->

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>