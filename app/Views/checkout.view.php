<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center"><?=esc($title)?></h1>
<?php include __DIR__ . '/includes/flash.message.inc.php';?>

<?php if(!empty($_SESSION['Cart'])) : ?>
  <table class="my_table">
    <caption>Shopping Cart</caption>
    <thead>
      <tr>
        <th>Item</th>
        <th>Unit Price</th>
        <th>Quantity</th>
        <th>Line Price</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($_SESSION['Cart'] as $item) : ?>
        <tr>
            <td><?=esc($item['title'])?></td>
            <td>$<?=esc(number_format($item['price'] ?? 0,2))?></td>
            <td><?=esc($item['qty'])?></td>
            <td>$<?=esc(number_format($item['line_price'] ?? 0,2))?></td>
        </tr>
      <?php endforeach; ?>
      <tr>
        <td colspan="3" class="text_align_right grey_bg">Sub Total</td>
        <td class="grey_bg">$<?=esc(number_format($subtotal, 2))?></td>
      </tr>
      <tr>
        <td colspan="3" class="text_align_right grey_bg">GST</td>
        <td class="grey_bg">$<?=esc(number_format($gst, 2))?></td>
      </tr>
      <tr>
        <td colspan="3" class="text_align_right grey_bg">PST</td>
        <td class="grey_bg">$<?=esc(number_format($pst, 2))?></td>
      </tr>
      <tr>
        <td colspan="3" class="text_align_right grey_bg">Total</td>
        <td class="grey_bg">$<?=esc(number_format($total, 2))?></td>
      </tr>
      <tr>
        <td colspan="4">
        <form
          name="checkout_form"
          id="checkout_form"
          action="/?p=process_checkout"
          method="post"
          autocomplete = "on"
          novalidate>

          <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
          <fieldset>
            <legend>Payment Information</legend>
            <p><span class="req_icon">*</span><em> indicates a required field.</em></p>

            <p>
            <label for="card_name"><span class="req_icon">*</span>Name on Card: </label>
            <input type="text" 
                    name="card_name" 
                    id="card_name"
                    placeholder="Please enter your name on your card"
                    title="Please enter your name on your card"
                    value="<?=esc($post['card_name'] ?? '')?>"/>
            <span class="error"><em><?=esc($errors['card_name'][0] ?? '')?></em></span>
            </p>

            <p>
            <label for="card_no"><span class="req_icon">*</span>Card Number: </label>
            <input type="text" 
                    name="card_no" 
                    id="card_no"
                    placeholder="Please enter your card no"
                    value="<?=esc($post['card_no'] ?? '')?>"/>
            <span class="error"><em><?=esc($errors['card_no'][0] ?? '')?></em></span>
            </p>

            <p>
            <label for="card_expiry"><span class="req_icon">*</span>Expiry Date: </label>
            <input type="text" 
                    name="card_expiry" 
                    id="card_expiry"
                    placeholder="Please enter your card expiry date"
                    title="Format is MMYY"
                    value="<?=esc($post['card_expiry'] ?? '')?>"/>
            <span class="error"><em><?=esc($errors['card_expiry'][0] ?? '')?></em></span>
            </p>

            <p>
            <label for="card_cvv"><span class="req_icon">*</span>CVV: </label>
            <input type="text" 
                    name="card_cvv" 
                    id="card_cvv"
                    placeholder="Please enter your card cvv"
                    value="<?=esc($post['card_cvv'] ?? '')?>"/>
            <span class="error"><em><?=esc($errors['card_cvv'][0] ?? '')?></em></span>
            </p>

            <p><a class="btn primary" href="/?p=collection">Continue Shopping</a>
            <input type="submit" value="Pay Now" />
            </p>
          </fieldset><!-- end of checkout fieldset -->

        </form> <!-- end of checkout form -->

        </td>
      </tr>

    </tbody>
  </table>
<?php else : ?>
  <h2> Your cart is empty.</h2>
  <a href="?p=collection" title="Our Collection">View our Collection</a>
<?php endif; ?>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>