<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center"><?=esc($title)?></h1>

<?php include __DIR__ . '/includes/flash.message.inc.php';?>

<table class="my_table">
  <thead>
    <tr>
      <th>Company Info</th>
      <th>User Info</th>
      <th>Order Info</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        SavingEverAfter
      </td>
      <td>
        <strong>Name: </strong><?=esc($orderDetails[0]['first_name'].','.$orderDetails[0]['last_name'] )?><br>
        <strong>Street: </strong><?=esc($orderDetails[0]['street'])?><br>
        <strong>City: </strong><?=esc($orderDetails[0]['city'])?><br>
        <strong>Province: </strong><?=esc($orderDetails[0]['province'])?><br>
        <strong>Postal Code: </strong><?=esc($orderDetails[0]['postal_code'])?><br>
        <strong>Email: </strong><?=esc($orderDetails[0]['email'])?>	
      </td>
      <td>
        <strong>Order Number: </strong> 2022-<?=esc($orderDetails[0]['id'])?><br>
        <strong>Date: </strong> <?=esc($orderDetails[0]['created_at'])?> <br>
        <strong>Credit Card: </strong> ***********<?=esc($orderDetails[0]['card_no'])?> <br>
        <strong>Auth Code: </strong> <?=esc($orderDetails[0]['auth_code'])?> <br>
        <strong>Charged to Card: </strong> $<?=esc(number_format($total, 2))?>
      </td>
    </tr>

  </tbody>
</table>
<table class="my_table">
  <thead>
    <tr>
      <th>Item</th>
      <th>Unit Price</th>
      <th>Quantity</th>
      <th>Line Price</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($orderDetails as $item) : ?>
      <tr>
          <td><?=esc($item['title'])?></td>
          <td>$<?=esc(number_format($item['price'] ?? 0,2))?></td>
          <td><?=esc($item['qty'])?></td>
          <td>$<?=esc(number_format($item['line_price'] ?? 0,2))?></td>
      </tr>
    <?php endforeach; ?>
    <tr>
      <td colspan="3" class="text_align_right grey_bg">Sub Total</td>
      <td class="grey_bg">$<?=esc(number_format($subtotal, 2))?></td>
    </tr>
    <tr>
      <td colspan="3" class="text_align_right grey_bg">GST</td>
      <td class="grey_bg">$<?=esc(number_format($gst, 2))?></td>
    </tr>
    <tr>
      <td colspan="3" class="text_align_right grey_bg">PST</td>
      <td class="grey_bg">$<?=esc(number_format($pst, 2))?></td>
    </tr>
    <tr>
      <td colspan="3" class="text_align_right grey_bg">Total</td>
      <td class="grey_bg">$<?=esc(number_format($total, 2))?></td>
    </tr>

  </tbody>
</table>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>