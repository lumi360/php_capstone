<?php include __DIR__ . '/includes/adm_header.inc.php';?>
<div class="row">
  <div class="col-12">
    <?php include __DIR__ . '/includes/flash.message.inc.php';?>

    <br/>
    <h1>Clothing</h1>
    <br/>
    <p><a class="btn btn-success" href="/admin?p=clothing_add" style="float:left">Add an outfit</a> 
    <form class="form form-inline" 
          action="/admin" style="float:right">
          <input type="hidden" name="p" value="clothing" />
          <input type="text" name="search" placeholder="Search outfits"/>
          <button>Search</button>
        </form></p>
    <p class="clear">&nbsp;</p>
    <br/>
    <input type="hidden" id="csrf" value="<?=getCsrfToken()?>" />

    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">Clothing ID</th>
          <th scope="col">Title</th>
          <th scope="col">Designer</th>
          <th scope="col">Fabric</th>
          <th scope="col">Colour</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($clothingInfo as $row) : ?>
          <tr>
            <td><?=esc($row['id'])?></td>
            <td><?=esc(format_Label($row['title']))?></td>
            <td><?=esc(format_Label($row['designer']))?></td>
            <td><?=esc(format_Label($row['fabric']))?></td>
            <td><?=esc(format_Label($row['colour']))?></td>
            <td>
                <a class="btn btn-primary btn-sm" 
                href="/admin?p=clothing_edit&clothing_id=<?=esc($row['id'])?>">Edit</a>
                &nbsp;
                <a class="remove_item btn btn-danger btn-sm" data-id="<?=esc($row['id'])?>" 
                href="#">Delete</a>
            </td>
          </tr>
        <?php endforeach; ?> 

      </tbody>
    </table>
    <br/>
  </div>     
</div>
<?php include __DIR__ . '/includes/adm_footer.inc.php';?>

