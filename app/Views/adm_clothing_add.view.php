<?php include __DIR__ . '/includes/adm_header.inc.php';?>
<div class="row">
  <div class="col-12">
    <?php include __DIR__ . '/includes/flash.message.inc.php';?>

    <br/>
    <h1> Add Clothing Item</h1>
    <br/>
    <p><a class="btn btn-warning" href="/admin?p=clothing" style="float:left">Back</a> 
    <p class="clear">&nbsp;</p>

    <br/>
    <form
      name="clothing_add_form"
      id="clothing_add_form"
      action="/admin?p=process_clothing_add"
      method="post"
      autocomplete = "on"
      enctype="multipart/form-data"
      novalidate>

      <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
      <fieldset>
        <legend>Add Item</legend>

        <div class="form-group  ">
          <label for="title">Title</label>
          <input type="text" name="title" id="title" class="form-control" 
          placeholder = "Please enter clothing title" value="<?=esc($post['title'] ?? '')?>" />
          <span class="error"><em><?=esc($errors['title'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="image">Clothing Image</label><br />
          <!--<img class="thumb" src="/images/" 
          alt="" />-->
          <input type="file" name="image" id="image" class="form-control"  />
          <span class="error"><em><?=esc($errors['image'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="colour">Colour</label>
          <input type="text" name="colour" id="colour" class="form-control" 
          placeholder = "Please enter clothing colour" value="<?=esc($post['colour'] ?? '')?>" />
          <span class="error"><em><?=esc($errors['colour'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="size">Size</label>
          <input type="text" name="size" id="size" class="form-control" 
          placeholder = "Please enter clothing size e.g. 2,4,6 etc." value="<?=esc($post['size'] ?? '')?>" />
          <span class="error"><em><?=esc($errors['size'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="price">Price ($)</label>
          <input type="text" name="price" id="price" class="form-control" 
          placeholder = "Please enter clothing price" value="<?=esc(number_format($post['price'] ?? 0,2))?>" />
          <span class="error"><em><?=esc($errors['price'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="summary">Short Description</label>
          <input type="text" name="summary" id="summary" class="form-control" 
          placeholder = "Please enter clothing summary" value="<?=esc($post['summary'] ?? '')?>" />
          <span class="error"><em><?=esc($errors['summary'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="long_desc">Long Description</label>
          <textarea class="form-control" id="long_desc" name="long_desc" 
          placeholder = "Please enter clothing long description"><?=esc($post['long_desc'] ?? '')?></textarea>
          <span class="error"><em><?=esc($errors['long_desc'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="stock_count">Stock Count</label>
          <input type="text" name="stock_count" id="stock_count" class="form-control" 
          placeholder = "Please enter clothing stock amount" value="<?=esc($post['stock_count'] ?? '')?>" />
          <span class="error"><em><?=esc($errors['stock_count'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="quality">Quality</label><br />
            <select class="form-control" name="quality" id="quality">
              <option value="">Select Quality</option>
              <option  value="VERY GOOD" <?php if(!empty($post['quality']) && $post['quality'] == "VERY GOOD") : ?>
                selected<?php endif; ?>>VERY GOOD</option>
              <option  value="GOOD" <?php if(!empty($post['quality']) && $post['quality'] == "GOOD") : ?>
                selected<?php endif; ?>>GOOD</option>
              <option  value="OK" <?php if(!empty($post['quality']) && $post['quality'] == "OK") : ?>
                selected<?php endif; ?>>OK</option>
              <option  value="POOR" <?php if(!empty($post['quality']) && $post['quality'] == "POOR") : ?>
                selected<?php endif; ?>>POOR</option>
            </select>
          <span class="error"><em><?=esc($errors['quality'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="designer">Designer</label><br />
            <select class="form-control" name="designer" id="designer">
              <option value="">Select Designer</option>
              <?php foreach($designers as $Data) : ?>   
                <?php foreach($Data as $key => $value) : ?>   
                  <option value="<?=esc($value)?>" <?php if(!empty($post['designer']) && $post['designer'] == $value) : ?>
                selected<?php endif; ?>><?=esc(format_Label($value))?></option>
                <?php endforeach; ?>   
              <?php endforeach; ?>   
            </select>
          <span class="error"><em><?=esc($errors['designer'][0] ?? '')?></em></span>
        </div>

        <div class="form-group  ">
          <label for="fabric">Fabric</label><br />
            <select class="form-control" name="fabric" id="fabric">
              <option value="">Select Fabric</option>
              <?php foreach($fabrics as $Data) : ?>   
                <?php foreach($Data as $key => $value) : ?>   
                  <option value="<?=esc(str_replace(' ','-',$value))?>" <?php if(!empty($post['fabric']) && $post['fabric'] == $value) : ?>
                selected<?php endif; ?>><?=esc(format_Label($value))?></option>
                <?php endforeach; ?>   
              <?php endforeach; ?> 
            </select>
          <span class="error"><em><?=esc($errors['fabric'][0] ?? '')?></em></span>
        </div>
        <br/>

        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </fieldset><!-- end of add clothing fieldset -->

    </form> <!-- end of add clothing form -->
    <br/>
  </div>     
</div>
<?php include __DIR__ . '/includes/adm_footer.inc.php';?>

