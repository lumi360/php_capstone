  <div class="row" id="myfooter">
      <div class="col text-center pt-4 pb-2">
        <p>By ajayi-o51@webmail.uwinnipeg.ca</p>
        <p>Copyright: &copy; 2022. All rights reserved</p>
      </div>
    </div>
  </div><!-- /.container -->

  <!-- load jquery only on clothing page -->
  <?php if($title === "Admin - Clothing") : ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script>

      $(document).ready(function(){
          $('a.remove_item').each(function(){
            $(this).click(function(e){
              e.preventDefault();
              console.log(document.getElementById('csrf').value);
              if(confirm('Are you sure you want to delete this item?'))
              {
                  var data = {};
                  data.delete = $(this).data('id');
                  data.csrf = document.getElementById('csrf').value;
                  $.post('/admin?p=clothing_delete', data, function(response){});
                  window.location = '/admin?p=clothing';
                  //window.location = '/admin?p=clothing&delete='+$(this).data('id')+'&csrf='+document.getElementById('csrf').value;
              }// endif
            });
          });
      });

    </script>
  <?php endif; ?>
  
</body>
</html>