<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?=esc($title)?></title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
  <style>
      #myfooter
      {
        background: #999;
      }
      .logout{
        margin: 5px;
        background: #f00;
        border-radius: 5px;
      }
      .msg_box{
        color: #f00;
        font-weight: 700;
        font-size: 1.2em;
        background-color: #F8D7D9;
        border-radius: 7px;
        padding: 10px 60px;
        margin: 0 auto 40px;
        text-align: center;
      }

      .msg_box_good{
        color: rgb(16, 153, 16);
        font-size: 1.2em;
        font-weight: 700;
        background-color: #D1E7DD;
        border-radius: 7px;
        padding: 10px 60px;
        margin: 0 auto 40px;
        text-align: center;
      }

      .form-group{
        margin-bottom: 1rem;
      }
      .error{
        color:#f00;
        font-size:0.9em;
        font-weight: 700;
      }

      img.thumb {
        max-width: 85px;
        height: auto;
        margin: 20px;
      }

      fieldset label{
        font-weight:700;
      }
  </style>
</head>
<body>
  <nav class="navbar navbar-expand navbar-dark bg-primary">
    <div class="container">
      <a class="navbar-brand" href="/admin">SavingEverAfter Admin</a>
      <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="/admin">Dashboard</a></li>
        <li class="nav-item"><a class="nav-link" href="/admin?p=clothing">Clothing</a></li>
        <li class="nav-item"><a class="nav-link" href="/admin?p=users">Users</a></li>
        <li class="nav-item"><a class="nav-link" href="/admin?p=orders">Orders</a></li>
        <li class="nav-item"><a class="nav-link" href="/admin?p=construction">Designers</a></li>
        <li class="nav-item"><a class="nav-link" href="/admin?p=construction">Genres</a></li>
        <li>
          <form action="/?p=process_logout" id="util_logout" method="post">
            <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
            <input type="submit" name="logout" value="logout" class="nav-item logout" />
          </form>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container">