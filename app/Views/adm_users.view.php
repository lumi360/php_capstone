<?php include __DIR__ . '/includes/adm_header.inc.php';?>
<div class="row">
  <div class="col-12">
    <?php include __DIR__ . '/includes/flash.message.inc.php';?>

    <br/>
    <h1>Users</h1>
    <br/>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">User ID</th>
          <th scope="col">Name (Last, First)</th>
          <th scope="col">Email</th>
          <th scope="col">City</th>
          <th scope="col">Role</th>
          <th scope="col">Registration Date</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($userInfo as $row) : ?>
          <tr>
            <td><?=esc($row['id'])?></td>
            <td><?=esc($row['last_name'])?>, <?=esc($row['first_name'])?></td>
            <td><?=esc($row['email'])?></td>
            <td><?=esc($row['city'])?></td>
            <td><?=esc($row['is_admin'] === 0 ? "Regular" : "Admin")?></td>
            <td><?=esc($row['created_at'])?></td>
          </tr>
        <?php endforeach; ?> 

      </tbody>
    </table>
    <br/>
  </div>     
</div>
<?php include __DIR__ . '/includes/adm_footer.inc.php';?>
