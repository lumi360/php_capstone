<?php include __DIR__ . '/includes/header.inc.php';?>

<h1 id="motto_header" class="position_center"><?=esc($title)?></h1>

<?php include __DIR__ . '/includes/flash.message.inc.php';?>


<?php if(!empty($_SESSION['Cart'])) : ?>
  <table class="my_table">
    <caption>Shopping Cart</caption>
    <thead>
      <tr>
        <th>Item</th>
        <th>Unit Price</th>
        <th>Quantity</th>
        <th>Line Price</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($_SESSION['Cart'] as $item) : ?>
        <tr>
            <td><?=esc($item['title'])?></td>
            <td>$<?=esc(number_format($item['price'] ?? 0,2))?></td>
            <td><?=esc($item['qty'])?></td>
            <td>$<?=esc(number_format($item['line_price'] ?? 0,2))?></td>
            <td>
              <form action="/?p=handle_cart" method="post" autocomplete="off" novalidate>
                <input type="hidden" name="CsrfToken" value="<?=getCsrfToken()?>" />
                <input type="submit" value="Remove" />
                <input type="hidden" name="remove_id" value="<?=esc($item['id'])?>" />
              </form>
            </td>
        </tr>
      <?php endforeach; ?>
      <tr>
        <td colspan="4" class="text_align_right grey_bg">Sub Total</td>
        <td class="grey_bg">$<?=esc(number_format($subtotal, 2))?></td>
      </tr>
      <tr>
        <td colspan="4" class="text_align_right grey_bg">GST</td>
        <td class="grey_bg">$<?=esc(number_format($gst, 2))?></td>
      </tr>
      <tr>
        <td colspan="4" class="text_align_right grey_bg">PST</td>
        <td class="grey_bg">$<?=esc(number_format($pst, 2))?></td>
      </tr>
      <tr>
        <td colspan="4" class="text_align_right grey_bg">Total</td>
        <td class="grey_bg">$<?=esc(number_format($total, 2))?></td>
      </tr>
      <tr>
        <td colspan="5" class="text_align_right">
          <a class="btn primary" href="/?p=collection">Continue Shopping</a>&nbsp;&nbsp;
          <a class="btn danger" href="/?p=handle_cart&clear=1">Clear cart</a>&nbsp;&nbsp;
          <a class="btn success" href="/?p=checkout">Checkout now</a>
        </td>
      </tr>

    </tbody>
  </table>
<?php else : ?>
  <h2> Your cart is empty.</h2>
  <a href="?p=collection" title="Our Collection">View our Collection</a>
<?php endif; ?>

<?php  include __DIR__ . '/includes/footer.inc.php'; ?>