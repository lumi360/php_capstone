<?php include __DIR__ . '/includes/adm_header.inc.php';?>
<div class="row">
  <div class="col-12">
    <?php include __DIR__ . '/includes/flash.message.inc.php';?>

    <br/>
    <h1>Orders</h1>
    <br/>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">Order Date</th>
          <th scope="col">Name (Last, First)</th>
          <th scope="col">Email</th>
          <th scope="col">No. of Items</th>
          <th scope="col">Subtotal</th>
          <th scope="col">Taxes</th>
          <th scope="col">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($orders as $row) : ?>
          <tr>
            <td><?=esc($row['order_date'])?></td>
            <td><?=esc($row['last_name'])?>, <?=esc($row['first_name'])?></td>
            <td><?=esc($row['email'])?></td>
            <td><?=esc($row['no_of_items'])?></td>
            <td>$<?=esc(number_format($row['sub_total'],2))?></td>
            <td>$<?=esc(number_format($row['taxes'],2))?></td>
            <td>$<?=esc(number_format($row['total'],2))?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <br/>
  </div>     
</div>
<?php include __DIR__ . '/includes/adm_footer.inc.php';?>
