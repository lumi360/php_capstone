<?php

$title = "Admin - Clothing";

$clothing = new Clothing();
$clothingInfo = $clothing->getFullClothingData($dbh);

if(!empty($_GET['search']))
{
  //replace clothinginfo with data using search value. if db not null
  $clothingInfoFromSearch = $clothing->getFullClothingDataBySearch($dbh,$_GET['search']);
  if(!empty($clothingInfoFromSearch)){
    $clothingInfo = $clothingInfoFromSearch;
  }
  else{
    $_SESSION['flashMsg']['error'] = "No results for search '".$_GET['search']. "'. Default view returned";
  }

}

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

//compile data
$data = compact('title','clothingInfo','flashMsg');

//load data in view
view('adm_clothing',$data);