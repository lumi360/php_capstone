<?php

//Ensure page is accessed via post alone
if('POST' !== $_SERVER['REQUEST_METHOD']) 
{
  //http_status_code(405);
  header("HTTP/1.1 405 Unsupported method detected");

  die('Unsupported method detected');
}
else
{
  if(!validateCsrfToken($_POST['CsrfToken']))
  {
    die('CSRF TOKEN MISMATCH DETECTED!');
  }
  //initiated validator class
  $validatorObj = new App\Lib\Classes\Validator($_POST);
  //required array for form fields
  $required_arr = ['first_name', 'last_name', 'email', 'city', 'phone', 'street',
  'country', 'postal_code', 'province', 'password', 'confirm_password'];
  
  $validatorObj->check_Required_Fields($required_arr);

  $validatorObj->validate_names('first_name');
  $validatorObj->validate_names('last_name');
  $validatorObj->validate_Phone('phone');
  $validatorObj->validate_city('city');
  $validatorObj->validate_Email('email');
  $validatorObj->validate_Street('street');
  $validatorObj->validate_Country('country');
  $validatorObj->validate_Province('province');
  $validatorObj->validate_Password_Field('password');
  $validatorObj->compare_Passwords('password','confirm_password');
  $validatorObj->validate_PostalCode('postal_code');

  
  $hashed_user_password = password_hash($_POST['password'],PASSWORD_DEFAULT);

  $validatedArr = $validatorObj->get_validator_array();
  //Set hashed password to session
  $validatedArr['password'] = $hashed_user_password;
  
  $_SESSION['register_form_arr'] = json_encode($validatedArr);
  //$_SESSION['register_form_arr'] = addFormValuesToSession($hashed_user_password,$_POST);

  //check if email is unique
  $isUserEmailUnique = checkUserEmailIsUnique($_POST['email']);
  if(!$isUserEmailUnique)
  {
    //$register_result = "Sorry, the email you provided already exists on our platform. ";
    $_SESSION['flashMsg']['error'] = "Sorry, the email you provided already exists on our platform. ";
    header('Location: ?p=register');
    die;
  }

  // if no errors
  if(count($validatorObj->validator_errors()) == 0) 
  {
    //Add file to handle logic to insert user 
    $id = insertUserData($hashed_user_password);
    if($id > 0) 
    {
      $_SESSION['authorized'] = 1;
      $_SESSION['profile_id']  = $id;
      $_SESSION['flashMsg']['success'] = "You have been successfully registered!!";
      header('Location: ?p=profile');//Redirect to profile page after successful insert.
      die;
    } 
    else{
      //$register_result = "Sorry, an error occurred submitting your information";
      $_SESSION['flashMsg']['error'] = "Sorry, an error occurred submitting your information";
      header('Location: ?p=register');
      die;
    }
  }
  else
  {
    $_SESSION['errors'] = json_encode($validatorObj->validator_errors());
    header('Location: ?p=register');
    die;
  }
}