<?php

$title = "SavingEverAfter - Checkout";
$page_index = "";
$subtotal=0;
$gst=0;
$pst=0;
$total=0;

if(empty($_SESSION['authorized']) ) {
  $_SESSION['flashMsg']['error'] = 'Please login to complete transaction';
  header('Location: ?p=login');
  die;
}

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

$errors =  !empty($_SESSION['errors']) ? json_decode($_SESSION['errors'],true) : [];
unset($_SESSION['errors']);

$post =  !empty($_SESSION['checkout_form']) ? json_decode($_SESSION['checkout_form'],true) : [];
unset($_SESSION['checkout_form']);

if(!empty($_SESSION['Cart'])){
  foreach($_SESSION['Cart'] as $line_item){
      $subtotal+=$line_item['line_price'];   
  }
  $gst=0.05*$subtotal;
  $pst=0.07*$subtotal;
  $total=$subtotal+$gst+$pst;
}

//compile data
$data = compact('title','page_index','flashMsg','gst','pst','total','subtotal','post','errors');

//load data in view
view('checkout',$data);