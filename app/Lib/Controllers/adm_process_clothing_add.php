<?php

//Ensure page is accessed via post alone
if('POST' !== $_SERVER['REQUEST_METHOD']) 
{
  //http_status_code(405);
  header("HTTP/1.1 405 Unsupported method detected");
  die('Unsupported method detected');
}
else
{
  //validate csrf token on form submission
  if(!validateCsrfToken($_POST['CsrfToken']))
  {
    die('CSRF TOKEN MISMATCH DETECTED!');
  }

  //initiated validator class
  $validatorObj = new App\Lib\Classes\Validator($_POST);

  //required array for form fields
  $required_arr = ['title', 'colour', 'size', 'price', 'summary', 'long_desc',
  'stock_count', 'quality', 'designer', 'fabric'];

  //validate required fields
  $validatorObj->check_Required_Fields($required_arr);

  $validatorObj->validate_digits('size');
  $validatorObj->max_string_length_check('size',2);
  $validatorObj->validate_digits('stock_count');
  $validatorObj->max_string_length_check('stock_count',2);
  $validatorObj->validate_title('title');
  $validatorObj->validate_title('colour');
  $validatorObj->validate_price('price');
  $validatorObj->validate_TEXT('summary');
  $validatorObj->max_string_length_check('summary',50);
  $validatorObj->validate_TEXT('long_desc');
  $validatorObj->max_string_length_check('long_desc',150);
  
  //validate image upload
  $validatorObj->validate_image_file_upload('image');
  
  $cl = new Clothing();
  
  //get quality enum array from db
  $enums = $cl->getQualityEnums($dbh);
  $designers = $cl->getAllDesigners($dbh);
  $fabrics = $cl->getAllFabrics($dbh);

  $designersArr =[];
  foreach($designers as $Data){
    foreach($Data as $key=>$value){
      //push designers into array for dropdown validation
      array_push($designersArr,$value);
    }
  }
  
  $fabricsArr =[];
  foreach($fabrics as $Data){
    foreach($Data as $key=>$value){
      //push fabrics into array for dropdown validation
      array_push($fabricsArr,$value);
    }
  }
  
  //validate all dropdown info
  $validatorObj->validate_Dropdowns('quality',$enums);
  $validatorObj->validate_Dropdowns('designer',$designersArr);
  $validatorObj->validate_Dropdowns('fabric',$fabricsArr);

  //Fetch validated array of form values
  $validatedArr = $validatorObj->get_validator_array();

  //Add form values to session to make values sticky
  $_SESSION['add_clothing_form'] = json_encode($validatedArr); 

  //if no errors continue with process
  if(count($validatorObj->validator_errors()) == 0) 
  {
    $slug = str_replace(" ","-",$validatedArr['title']);
    $clothingDataWithSlug = $cl->getFullClothingDataBySlugName($dbh,$slug);
    if(!empty($clothingDataWithSlug))
    {
      $_SESSION['flashMsg']['error'] = "Title already taken. Please pick a different title";
      header('Location: /admin?p=clothing_add');
      die();
    }

    //Move temp file to images folder and return formatted file name
    $imageFileName = moveUploadedImageFile('image',$validatedArr['title']);
    if(empty($imageFileName)){
      $_SESSION['flashMsg']['error'] = "Sorry, image upload failed. Contact support";
      header('Location: /admin?p=clothing_add');
      die();
    }

    $clothingItem = [
      'title' => $validatedArr['title'],
      'slug' => $slug,
      'long_desc' => "<p>".$validatedArr['long_desc']."</p>",
      'summary' => $validatedArr['summary'],
      'colour' => $validatedArr['colour'],
      'fabric' => $validatedArr['fabric'],
      'size' => $validatedArr['size'],
      'size_type' => "US",
      'price' => $validatedArr['price'],
      'stock_count' => $validatedArr['stock_count'],
      'quality' => $validatedArr['quality'],
      'designer' => $validatedArr['designer'],
      'image_name' => $imageFileName
    ];

    $dbResult =$cl->insertNewClothingItem($dbh,$clothingItem);
    if($dbResult> 0)
    {
      //successful insert
      $_SESSION['flashMsg']['success'] = "Clothing item added successfully";
      header('Location: /admin?p=clothing_add');
      die();
    }
    else{
      //insert failed
      $_SESSION['flashMsg']['error'] = "Sorry, an error occurred treating your request. Contact support";
      header('Location: /admin?p=clothing_add');
      die();
    }

  }
  else
  {
    $_SESSION['errors'] = json_encode($validatorObj->validator_errors());
    $_SESSION['flashMsg']['error'] = "Please check your form input";
    header('Location: /admin?p=clothing_add');
    die();
  }
 
}