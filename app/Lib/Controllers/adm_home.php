<?php

$title = "SavingEverAfter - Admin";

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

//combined array of log entries
$mergedLogData[]['event'] = [];
//file path for lof file
$fileLog = __DIR__ . '/../../../logs/events.log';
$admLog = new AdminLog();//model to get data for log entries


$dbLogData = $admLog->loadLast10DbLogEntries($dbh);
$fileLogData = getFileLogDataArr($fileLog);

//Add both db log entries and file log entries to combine log array
$mergedLogData = $dbLogData; 
foreach($fileLogData as $item)
{
  $mergedLogData[]['event'] =  $item;
}

//reorder the log array in descending order
arsort($mergedLogData);

//get filtered array of 10 entries to show in admin dashboard
$filteredLogData = array_splice($mergedLogData,0,10);


$dashboard = new Dashboard();
//Load Total User, Products and orders
$dashboardOverview = $dashboard->getTotalUser_Product_Orders($dbh);

$clothing = new Clothing();
//Load Maximum, Minimum and Average price for products (Clothing)
$clothingOverview = $clothing->getMinMaxAvgPriceClothing($dbh);

$order = new Order();
//Load Maximum, Minimum and Average price for products (Clothing)
$orderOverview = $order->getMinMaxAvgPriceOrders($dbh);

//compile data
$data = compact('title','filteredLogData','dashboardOverview','flashMsg','clothingOverview','orderOverview');

//load data in view
view('adm_index',$data);