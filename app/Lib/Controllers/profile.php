<?php

$title = "SavingEverAfter - Profile";
$page_index = "profile";
$profile_result = "";
$user_profile = [];

//ensure user is authorised before allowing to continue
if(empty($_SESSION['authorized']) ) {
  $_SESSION['flashMsg']['error'] = 'Please login to view profile';
  header('Location: ?p=login');
  die;
}

$profile_id = $_SESSION['profile_id'];
$user_profile = getUserByID($profile_id);
//if no data is returned display message to user
if(!$user_profile){
  //$profile_result = "No profile matches the id provided";
  $_SESSION['flashMsg']['error'] = 'You are not authorized to view that resource.';
  header('Location: ?p=login');
  die;
}

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

$data = compact('title','page_index','profile_result','user_profile' ?? "", 'flashMsg');

view('profile',$data);