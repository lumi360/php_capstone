<?php

$title = "SavingEverAfter - Collection Details";
$page_index = "Collection";
$detailResultMsg = "";

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

$cl = new Clothing();
$designers = $cl->getAllDesigners($dbh);
if(str_contains(strtolower($_SERVER['QUERY_STRING']), 'name'))
{
  //fetch slug value from url and run query based on category
  parse_str(strtolower($_SERVER['QUERY_STRING']), $query);
  $slugName = $query['name'];
  $slugName_formatted = str_replace('-',' ',$slugName);

  $detailView = $cl->getFullClothingDataBySlugName($dbh,$slugName);
  //var_dump($detailView);
  if(empty($detailView)){
    $detailResultMsg = "No results for ".format_Label($slugName_formatted);
  }else{
    $detailResultMsg = "Details of ". format_Label($slugName_formatted);
  }

}else{
  header('Location: ?p=collection');
  die();
}

//compile data
$data = compact('title','page_index','detailResultMsg','designers','detailView' ?? [],'flashMsg');

//load data in view
view('detail',$data);