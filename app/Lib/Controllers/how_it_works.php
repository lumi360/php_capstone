<?php

$title = "SavingEverAfter - How It Works";
$page_index = "How It Works";

//compile data
$data = compact('title','page_index');

//load data in view
view('how_it_works',$data);