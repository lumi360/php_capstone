<?php

//Ensure page is accessed via post alone
if('POST' !== $_SERVER['REQUEST_METHOD']) 
{
  //http_status_code(405);
  header("HTTP/1.1 405 Unsupported method detected");

  die('Unsupported method detected');
}
else
{

  if(!validateCsrfToken($_POST['CsrfToken']))
  {
    die('CSRF TOKEN MISMATCH DETECTED!');
  }

  if(!empty($_POST['logout'])) {
    unset($_SESSION['profile_id']);
    unset($_SESSION['is_admin']);
    unset($_SESSION['authorized']);
    unset($_SESSION['register_form_arr']);
    unset($_SESSION['checkout_form']);
    unset($_SESSION['CheckoutOk']);
    unset($_SESSION['Cart']);
    $_SESSION['flashMsg']['success'] = 'You have successfully logged out';
    header('Location: ?p=login');
    die;   
  }
  
  $_SESSION['flashMsg']['error'] = 'Are you sure you are logged in?';
  header('Location: ?p=login');
  die;   
}
