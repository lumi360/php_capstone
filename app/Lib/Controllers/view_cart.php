<?php

$title = "SavingEverAfter - Shopping Cart";
$page_index = "";
$subtotal=0;
$gst=0;
$pst=0;
$total=0;

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

if(!empty($_SESSION['Cart'])){
  foreach($_SESSION['Cart'] as $line_item){
      $subtotal+=$line_item['line_price'];   
  }
  $pst=0.07*$subtotal;
  $gst=0.05*$subtotal;
  $total=$subtotal+$gst+$pst;
}

//compile data
$data = compact('title','page_index','flashMsg','gst','pst','total','subtotal');

//load data in view
view('view_cart',$data);