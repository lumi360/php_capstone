<?php

if('POST' !== $_SERVER['REQUEST_METHOD']) die('Unsupported request method');

//validate csrf token on form submission
if(!validateCsrfToken($_POST['CsrfToken']))
{
  die('CSRF TOKEN MISMATCH DETECTED!');
}

//initiated validator class
$validatorObj = new App\Lib\Classes\Validator($_POST);

//required array for form fields
$required_arr = ['card_expiry', 'card_name', 'card_cvv', 'card_no'];

//validate required fields
$validatorObj->check_Required_Fields($required_arr);
$validatorObj->validate_digits('card_no');
$validatorObj->validate_digits('card_cvv');
$validatorObj->validate_digits_allow_zero_start('card_expiry');
$validatorObj->match_string_length_check('card_expiry',4);
$validatorObj->match_string_length_check('card_cvv',3);
$validatorObj->match_string_length_check('card_no',16);
$validatorObj->validate_title('card_name');

//Fetch validated array of form values
$validatedArr = $validatorObj->get_validator_array();

//Add form values to session to make values sticky
$_SESSION['checkout_form'] = json_encode($validatedArr); 

//if no errors continue with process
if(count($validatorObj->validator_errors()) == 0) 
{
  //start insertion of order details to orders and line items table
  
  $orderCl = new Order();
  $result = $orderCl->saveOrder($dbh,$_SESSION['profile_id'],$_SESSION['Cart'],$validatedArr['card_no']);
  if($result)
  {
    //Item saved successfully. redirect to success page
    $_SESSION['flashMsg']['success'] = "Thank you for your order.";
    $_SESSION['CheckoutOk'] = "1";
    $_SESSION['checkout_orderid'] = $result;
    unset($_SESSION['Cart']);
    header('Location: /?p=thankyou');
    die;
  }
  else{
    $_SESSION['flashMsg']['error'] = "Sorry your transaction could not be processed right now.";
    header('Location: /?p=checkout');
    die;
  }
}
else
{
  $_SESSION['errors'] = json_encode($validatorObj->validator_errors());
  $_SESSION['flashMsg']['error'] = "Please check your form inputs";
  header('Location: /?p=checkout');
  die;

}