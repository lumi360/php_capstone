<?php

$title = "SavingEverAfter - Contact Us";
$page_index = "Contact";

//compile data
$data = compact('title','page_index');

//load data in view
view('contact',$data);