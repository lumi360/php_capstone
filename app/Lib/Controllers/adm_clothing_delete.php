<?php

try
{
  if('POST' === $_SERVER['REQUEST_METHOD'] && !empty($_POST['delete']) && !empty($_POST['csrf'])) 
  {
    if(!validateCsrfToken($_POST['csrf']))
    {
      $_SESSION['flashMsg']['error'] = "Invalid token detected!";
      die();
    }

    //
    $clothing = new Clothing();
    $removeClothingItem = $clothing->deleteClothingItem($dbh,$_POST['delete']);
    if($removeClothingItem > 0)
    {
      $_SESSION['flashMsg']['success'] = "Clothing item successfully removed";

    }
    else
    {
      $_SESSION['flashMsg']['error'] = "Failed to remove clothing item.";

    }

  }else{
    $_SESSION['flashMsg']['error'] = "Invalid delete operation detected!";
  }

}
catch(Exception $e){
  $_SESSION['flashMsg']['error'] = "Error occurred treating delete request!";

}