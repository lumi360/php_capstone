<?php

$title = "SavingEverAfter - Register Today";
$page_index = "Register";

if(!empty($_SESSION['authorized']) )
{
  $_SESSION['flashMsg']['error'] = 'You are using an active profile';
  header('Location: ?p=profile');
  die;
}

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

//$register_result = !empty($_SESSION['register_result']) ? $_SESSION['register_result'] :  "";
//unset($_SESSION['register_result']);

$errors =  !empty($_SESSION['errors']) ? json_decode($_SESSION['errors'],true) : [];
unset($_SESSION['errors']);

$register_form_values_arr = !empty($_SESSION['register_form_arr']) ? json_decode($_SESSION['register_form_arr'],true) : [];
//if(!empty($register_form_values_arr)) dc($register_form_values_arr);
unset($_SESSION['register_form_arr']);

$data = compact('title','page_index','flashMsg','errors','register_form_values_arr');

view('register',$data);