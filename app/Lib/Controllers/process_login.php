<?php

//Ensure page is accessed via post alone
if('POST' !== $_SERVER['REQUEST_METHOD']) 
{
  //http_status_code(405);
  header("HTTP/1.1 405 Unsupported method detected");
  die('Unsupported method detected');
}
else
{
  if(!validateCsrfToken($_POST['CsrfToken']))
  {
    die('CSRF TOKEN MISMATCH DETECTED!');
  }
  $errors = [];
  //trim all post values
  foreach($_POST as $key => $value) 
  {
    $_POST[$key] = trim($value);
  }

  // The following fields are required
  $required_arr = ['email', 'password'];

  // Validate required fields from required array
  foreach($required_arr as $post_key) 
  {
    if(empty($_POST[$post_key])) 
    {
      $label = format_Label($post_key);
      $errors[$post_key][] = $label . " is required";
    }
  }
  
  // Validate email field
  if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
  {
    $errors['email'][] =  strlen($_POST['email'])> 100 ?'Email can not be more than 100 characters':
    'Email field must be a valid email';
  }

  // if no errors
  if(count($errors) == 0) 
  {
    //verify user email and password with db info. If true profile id is added to session
    $isUserOk = validateUserLoginCredentials($_POST['email'],$_POST['password']);
    if($isUserOk)
    {
      //check if user is admin. If true set admin session and  redirect to admin dashboard
      $isUserAdmin = checkIfUserIsAdmin($_POST['email'],$dbh);
      if($isUserAdmin)
      {
        $_SESSION['is_admin'] = 1;
        $_SESSION['authorized'] = 1;
        $_SESSION['flashMsg']['success'] = "You have successfully logged in!!";
        header('Location: /admin');
        die();
      }
      //Assign flash messages
      $_SESSION['flashMsg']['success'] = "You have successfully logged in!";
      //set user as authorized
      $_SESSION['authorized'] = 1;
      header('Location: ?p=profile');//Redirect to profile page after successful login.
      die;
    }
    else{
      
      $_SESSION['flashMsg']['error'] = "Sorry, those credentials don't match our records";
      header('Location: ?p=login');
      die;
    }
  }
  else
  {
    //errors exist. redirect to login
    $_SESSION['errors'] = json_encode($errors);
    header('Location: ?p=login');
    die;
  }

}

