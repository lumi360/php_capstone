<?php

$title = "SavingEverAfter - Our Collection";
$page_index = "Collection";
$collectionResultMsg = "";

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

$cl = new Clothing();
$designers = $cl->getAllDesigners($dbh);

if(str_contains(strtolower($_SERVER['QUERY_STRING']), 'category'))
{
  //fetch category value from url and run query based on category
  parse_str(strtolower($_SERVER['QUERY_STRING']), $query);
  $designer = $query['category'];
  $designer = str_replace('-',' ',$designer);

  $fullClothingList  = $cl->getFullClothingDataByDesigner($dbh,$designer);
  if(empty($fullClothingList)){
    $collectionResultMsg = "No results for category ".format_Label($designer);
  }else{
    $collectionResultMsg = "Outfits by ". format_Label($designer);
  }

}
else if(str_contains(strtolower($_SERVER['QUERY_STRING']), 'search'))
{
  //fetch search value from url and run query based on input
  parse_str(strtolower($_SERVER['QUERY_STRING']), $query);
  $search = $query['search'];

  $fullClothingList  = $cl->getFullClothingDataBySearch($dbh,$search);
  if(empty($fullClothingList)){
    $collectionResultMsg = "No search results for ".format_Label($search);
  }else{
    $collectionResultMsg = "Results for ". format_Label($search);
  }
}
else
{
  $fullClothingList  = $cl->getFullClothingData($dbh);
}


//compile data
$data = compact('title','page_index','designers','collectionResultMsg','fullClothingList','flashMsg');

//load data in view
view('collection',$data);