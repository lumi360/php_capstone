<?php

$title = "Thank you for your order";
$page_index = "Home";
$subtotal=0;
$gst=0;
$pst=0;
$total=0;

//check for flag set in session only after successful checkout
if(empty($_SESSION['CheckoutOk'])){
  header('Location: /?p=collection');
  die;
}

//unset checkout flag
unset($_SESSION['CheckoutOk']);

//get order id from session
$order_id =$_SESSION['checkout_orderid'];
unset($_SESSION['checkout_orderid']);


$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

$orderCl = new Order();

$orderDetails = $orderCl->getOrderById($dbh,$order_id);
// $orderDetails = $orderCl->getOrderById($dbh,3);

foreach($orderDetails as $line_item){
    $subtotal+=$line_item['line_price'];   
}
$pst=0.07*$subtotal;
$gst=0.05*$subtotal;
$total=$subtotal+$gst+$pst;

//compile data
$data = compact('title','page_index','flashMsg','orderDetails','subtotal','pst','gst','total');

//load data in view
view('thankyou',$data);