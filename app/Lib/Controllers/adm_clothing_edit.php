<?php

$title = "Admin - Edit Clothing";
$cl = new Clothing();

//This is GET view. Get clothing data based on clothing id
$clothingData = $cl->getFullClothingDataById($dbh,$_GET['clothing_id']);
if(empty($clothingData))
{
  $_SESSION['flashMsg']['error'] = "Clothing id is invalid";
  header('Location: /admin?p=clothing');
  die();
}

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

$errors =  !empty($_SESSION['errors']) ? json_decode($_SESSION['errors'],true) : [];
unset($_SESSION['errors']);

//$post = [];
$post =  !empty($_SESSION['edit_clothing_form']) ? json_decode($_SESSION['edit_clothing_form'],true) : [];
if(!empty($post) && $post['clothing_id'] !== $_GET['clothing_id']){
  $post = [];
}
unset($_SESSION['edit_clothing_form']);

if('POST' === $_SERVER['REQUEST_METHOD']) 
{
  //Form is submitted. Validate fields

  //validate csrf token on form submission
  if(!validateCsrfToken($_POST['CsrfToken']))
  {
    die('CSRF TOKEN MISMATCH DETECTED!');
  }

  //initiated validator class
  $validatorObj = new App\Lib\Classes\Validator($_POST);

  //required array for form fields
  $required_arr = ['title', 'colour', 'size', 'price', 'summary', 'long_desc',
  'stock_count', 'quality', 'designer', 'fabric'];

  //validate required fields
  $validatorObj->check_Required_Fields($required_arr);

  $validatorObj->validate_digits('size');
  $validatorObj->max_string_length_check('size',2);
  $validatorObj->validate_digits('stock_count');
  $validatorObj->max_string_length_check('stock_count',2);
  $validatorObj->validate_title('title');
  $validatorObj->validate_title('colour');
  $validatorObj->validate_price('price');
  $validatorObj->validate_TEXT('summary');
  $validatorObj->max_string_length_check('summary',50);
  $validatorObj->validate_TEXT('long_desc');
  $validatorObj->max_string_length_check('long_desc',150);
  
  //validate image upload if file is provided
  if(!empty($_FILES['image']['name']))
  {
    $validatorObj->validate_image_file_upload('image');
  }
  
  $cl = new Clothing();
  
  //get quality enum array from db
  $enums = $cl->getQualityEnums($dbh);
  $designers = $cl->getAllDesigners($dbh);
  $fabrics = $cl->getAllFabrics($dbh);

  $designersArr =[];
  foreach($designers as $Data){
    foreach($Data as $key=>$value){
      //push designers into array for dropdown validation
      array_push($designersArr,$value);
    }
  }
  
  $fabricsArr =[];
  foreach($fabrics as $Data){
    foreach($Data as $key=>$value){
      //push fabrics into array for dropdown validation
      array_push($fabricsArr,$value);
    }
  }
  
  //validate all dropdown info
  $validatorObj->validate_Dropdowns('quality',$enums);
  $validatorObj->validate_Dropdowns('designer',$designersArr);
  $validatorObj->validate_Dropdowns('fabric',$fabricsArr);

  //Fetch validated array of form values
  $validatedArr = $validatorObj->get_validator_array();

  //Add form values to session to make values sticky
  $_SESSION['edit_clothing_form'] = json_encode($validatedArr); 

  //if no errors continue with process
  if(count($validatorObj->validator_errors()) == 0) 
  {
    $slug = str_replace(" ","-",$validatedArr['title']);

    if(!empty($_FILES['image']['name']))
    {
      //Move temp file to images folder and return formatted file name. IF FILE UPLOADED
      $imageFileName = moveUploadedImageFile('image',$validatedArr['title']);
      if(empty($imageFileName)){
        $_SESSION['flashMsg']['error'] = "Sorry, image upload failed. Contact support";
        header('Location: /admin?p=clothing_edit&clothing_id='.$_GET['clothing_id']);
        die();
      }
    }
    else
    {
      $imageFileName = $clothingData['image_name'];
    }

    $clothingItem = [
      'title' => $validatedArr['title'],
      'slug' => $slug,
      'long_desc' => "<p>".$validatedArr['long_desc']."</p>",
      'summary' => $validatedArr['summary'],
      'colour' => $validatedArr['colour'],
      'fabric' => $validatedArr['fabric'],
      'size' => $validatedArr['size'],
      'size_type' => "US",
      'price' => $validatedArr['price'],
      'stock_count' => $validatedArr['stock_count'],
      'quality' => $validatedArr['quality'],
      'designer' => $validatedArr['designer'],
      'image_name' => $imageFileName,
      'updated_at'=> (new \DateTime('America/Winnipeg'))->format('Y-m-d H:i:s')
    ];

    $dbResult =$cl->updateClothingItem($dbh,$clothingItem,$_GET['clothing_id']);
    if($dbResult)
    {
      //successful update
      $_SESSION['flashMsg']['success'] = "Clothing item editted successfully";
      header('Location: /admin?p=clothing');
      die();
    }
    else{
      //update failed
      $_SESSION['flashMsg']['error'] = "Sorry, an error occurred treating your request. Contact support";
      header('Location: /admin?p=clothing_edit&clothing_id='.$_GET['clothing_id']);
      die();
    }

  }
  else
  {
    $_SESSION['errors'] = json_encode($validatorObj->validator_errors());
    $_SESSION['flashMsg']['error'] = "Please check your form input";
    header('Location: /admin?p=clothing_edit&clothing_id='.$_GET['clothing_id']);
    die();
  }
}

$designers = $cl->getAllDesigners($dbh);
$fabrics = $cl->getAllFabrics($dbh);

//compile data
$data = compact('title','flashMsg','errors','designers','fabrics','post','clothingData');

//load data in view
view('adm_clothing_edit',$data);