<?php

$notFoundMsg = "Page Not Found";
$title = "Page Not Found";
//compile data
$data = compact('title','notFoundMsg');
//load data in view
view('not_found',$data);