<?php

if(!empty($_GET['clear']))
{
  unset($_SESSION['Cart']);
  $_SESSION['flashMsg']['success'] = "Your cart has been cleared successfully";
  header('Location: /?p=view_cart');
  die();
}

if('POST' !== $_SERVER['REQUEST_METHOD']) die('Unsupported request method');

if(!validateCsrfToken($_POST['CsrfToken']))
{
  die('CSRF TOKEN MISMATCH DETECTED!');
}

if(!empty($_POST['remove_id'])){
  unset($_SESSION['Cart'][$_POST['remove_id']]);
  $_SESSION['flashMsg']['success'] = "The item has been removed";
  header('Location: /?p=view_cart');
  die();
}

//validate clothing id with db
$cl = new Clothing();

$clothingData = $cl->getFullClothingDataById($dbh,$_POST['clothing_id']);
if(empty($clothingData))
{
  $_SESSION['flashMsg']['error'] = "Clothing id mismatch";
  header('Location: /?p=detail&name='.$_POST['clothing_name']);
  die();
}

$item = array(
  'id' => $clothingData['id'],
  'title' => $clothingData['title'],
  'price' => $clothingData['price'],
  'qty' => $_POST['qty'],
  'line_price' => $clothingData['price'] *  $_POST['qty']
);

$_SESSION['Cart'][$clothingData['id']] = $item;

// flash message
$_SESSION['flashMsg']['success'] = "You have added an item to your cart!!";

header('Location: ' . $_SERVER['HTTP_REFERER']);
die;