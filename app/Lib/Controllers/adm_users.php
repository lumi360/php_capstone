<?php

$title = "Admin - Users";

$users = new User();
$userInfo = $users->getAllUsers($dbh);
//compile data
$data = compact('title','userInfo');

//load data in view
view('adm_users',$data);