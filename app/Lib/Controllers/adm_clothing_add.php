<?php

$title = "Admin - Add Clothing";

$cl = new Clothing();
$designers = $cl->getAllDesigners($dbh);
$fabrics = $cl->getAllFabrics($dbh);

$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

$errors =  !empty($_SESSION['errors']) ? json_decode($_SESSION['errors'],true) : [];
unset($_SESSION['errors']);

$post =  !empty($_SESSION['add_clothing_form']) ? json_decode($_SESSION['add_clothing_form'],true) : [];
unset($_SESSION['add_clothing_form']);


//compile data
$data = compact('title','flashMsg','errors','designers','fabrics','post');

//load data in view
view('adm_clothing_add',$data);