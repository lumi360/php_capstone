<?php

$title = "SavingEverAfter - Login";
$page_index = "";

//check if user is already logged in. redirect to profile
if(!empty($_SESSION['authorized']) && $_SESSION['authorized'] === 1 )
{
  //redirect to profile because user already logged in.
  $_SESSION['flashMsg']['success'] = "You are already logged in.";
  header('Location: ?p=profile');
  die;
}

//Load flash messages is any
$flashMsg = !empty($_SESSION['flashMsg']) ? $_SESSION['flashMsg'] :  [];
unset($_SESSION['flashMsg']);

//Load error messages is any
$errors =  !empty($_SESSION['errors']) ? json_decode($_SESSION['errors'],true) : [];
unset($_SESSION['errors']);

//$login_form_values_arr = !empty($_SESSION['login_form_arr']) ? json_decode($_SESSION['login_form_arr'],true) : [];
//unset($_SESSION['login_form_arr']);

//compile data
$data = compact('title','page_index','flashMsg','errors');

//load data in view
view('login',$data);