<?php

//namespace App\Lib\Interfaces;
//use App\Lib\Interfaces;
//require_once __DIR__ . '/../../Models/Model.php';
//require_once __DIR__ . '/../../Models/DbLogger.php';

class DatabaseLogger implements ILogger
{
  private $dbh;

  public function __construct($dbh)
  {
    $this->dbh = $dbh;
  }

  public function write($event)
  {
    Model::init($this->dbh);
    $dbModel = new DbLogger();
    $logArr = [
      "event" => $event
    ];

    $dbLogResp = $dbModel->insert($logArr);

    return $dbLogResp;
  }
}