<?php

namespace App\Lib\Classes;
class Validator
{
  private $array;
  private $errors = [];

  public function __construct(array $array)
  {
    foreach($array as $key => $value) 
    {
      $array[$key] = trim($value);
    }
    $this->array = $array;
  }

  public function check_Required_Fields($requiredArr)
  {
    foreach($requiredArr as $post_key) 
    {
      if(empty($this->array[$post_key])) 
      {
        $label = $this->format_label($post_key);
        $this->errors[$post_key][] = $label . " is required";
      }
    }
  }

  public function validate_names(string $field):void
  {
    // Validate first and last name field
    $pattern = '/^[A-z\s\-\,\']{1,30}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters.
       Must also be less than 30 characters';
    }
  }

  public function validate_TEXT(string $field):void
  {
    // Validate first and last name field
    $pattern = '/^[^?|<=>{}]*$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters or has incorrect format';
    }
  }

  public function validate_price(string $field):void
  {
    // Validate price field
    $pattern = '/^[1-9][\d]*\.?[0-9]{1,2}?$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters or has incorrect format';
    }
  }
  
  /**
   * validate_digits - checks field for digits only
   *
   * @param  mixed $field
   * @return void
   */
  public function validate_digits(string $field):void
  {
    // Validate and digit field
    $pattern = '/^[1-9]\d*$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters or has incorrect format';
    }
  }
  
  /**
   * validate_digits_allow_zero_start - check field for digits only (Zero start allowed)
   *
   * @param  mixed $field
   * @return void
   */
  public function validate_digits_allow_zero_start(string $field):void
  {
    // Validate and digit field
    $pattern = '/^[0-9]\d*$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters or has incorrect format';
    }
  }

  public function validate_title(string $field):void
  {
    // Validate title field
    $pattern = '/^[A-z\s\-]{1,30}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters.
       Must also be less than 30 characters';
    }
  }

  public function validate_city(string $field):void
  {
    // Validate city field
    $pattern = '/^[A-z\s\-\,\']{1,20}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters.
       Must also be less than 20 characters';
    }
  }

  public function validate_Phone(string $field):void
  {
    // Validate phone field
    $pattern = '/^\(?\d{3}(\)\s?|\-|\.|\s|)(\d{3})(\-|\.|\s|)\d{4}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters';
    }
  }

  public function validate_Email(string $field):void
  {
    // Validate email field
    if(!filter_var($this->array[$field], FILTER_VALIDATE_EMAIL)) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters';
    }
  }

  public function validate_PostalCode(string $field):void
  {
    // Validate postal code field
    $pattern = '/^[A-z][0-9][A-z](\s?)[0-9][A-z][0-9]$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters';
    }
  }

  public function validate_Street(string $field):void
  {
    // Validate street address field
    $pattern = '/^[A-z0-9\s\-\,\']{1,200}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' address contains invalid characters. (Max 200 Characters)';
    }
  }

  public function validate_Province(string $field):void
  {
    // Validate street address field
    $pattern = '/^[A-z\s]{1,100}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters. (Max 100 Characters)';
    }
  }

  public function validate_Country(string $field):void
  {
    // Validate street address field
    $pattern = '/^[A-z\s]{1,100}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' contains invalid characters. (Max 100 Characters)';
    }
  }

  public function validate_Password_Field(string $field):void
  {
    // Validate street address field
    $pattern = '/^(?=.*[A-z])(?=.*[\d])(?=.*[[:punct:]]).{8,}$/' ;
    if(!preg_match($pattern, $this->array[$field])) 
    {
      $this->errors[$field][]  = $this->format_label($field).' must have at least one number,letter and special character. (Min 8 characters)';
    }
  }

  public function compare_Passwords(string $password, string $confirm_password)
  {
    if($this->array[$password] != $this->array[$confirm_password]){
      $this->errors[$confirm_password][] = 'Passwords do not match';
    }

  }

  public function validate_Dropdowns(string $field, $referenceArr):void
  {
    if(!in_array($this->array[$field], $referenceArr))
    {
      $this->errors[$field][]  = $this->format_label($field).' has a data mismatch';
    }
  }

  public function min_string_length_check(string $field, int $length):void{
    if(strlen($this->array[$field]) < $length){
      //$this->errors[$field][]  = $this->format_label($field).' is less than '.$length. ' characters';
      $this->errors[$field][]  = $this->format_label($field).' can not be less than '.$length. ' characters';
    }
  }

  public function max_string_length_check(string $field, int $length):void{
    if(strlen($this->array[$field]) > $length){
      $this->errors[$field][]  = $this->format_label($field).' can not be more than '.$length. ' characters';
    }
  }

  public function match_string_length_check(string $field, int $length):void{
    if(strlen($this->array[$field]) != $length){
      $this->errors[$field][]  = $this->format_label($field).' must be exactly '.$length. ' characters';
    }
  }

  public function validate_image_file_upload(string $field, $max_size = 2000000):void{
    if(!empty($_FILES[$field]['name'])){
      //file uploaded
      $tmp_name = $_FILES[$field]['tmp_name'];

      $img_data = getimagesize($tmp_name);

      if(!$img_data) {
        $this->errors[$field][] = "The file you uploaded was not an image";
      } 
      else 
      {
        if($_FILES[$field]['size'] > $max_size){
          $this->errors[$field][] = "Please ensure file is less than 2MB";
          return;
        }
        $allowed = [
          'image/jpg',
          'image/jpeg'
        ];

        $mime = $img_data['mime'] ?? false;

        if(!$mime || !in_array($mime, $allowed)) {
          $this->errors[$field][] = 'Sorry, incompatible image format. JPEG or JPG only';
        }
      }

    }else{
      $this->errors[$field][] = "Please upload an image";
    }
  }



  public function validator_errors()
  {
    return $this->errors;
  }

  public function get_validator_array()
  {
    return $this->array;
  }

  /**
   * Replaces _ with space and makes first character capital
   */
  private function format_label(string $str):string
  {
    return ucwords(str_replace("_", " ", $str));
  }
}