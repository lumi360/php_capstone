<?php

/**
 * Load view with data
 *
 * @param string $view_name
 * @param array $data
 * @return void
 */
function view(string $view_name, array $data = []):void
{
  try {
    extract($data);
    // title and content now visible in this scope
    $path = __DIR__ . '/../app/Views/' . $view_name . '.view.php';
    if(!file_exists($path)) {
      throw new Exception('View ' . $path . ' not found.');
    }
    require($path);
  } catch(Exception $e) {
    echo $e->getMessage();
    die;
  }
}

function dd($var) {
  echo '<pre>';
  print_r($var);
  die;
}

function dc()
{
  if(defined('ENV') && ENV !== 'production') {
    if(func_num_args()) {
      $out = func_get_args();
    } else {
      $out = $GLOBALS;
    }
    $json = json_encode($out);
    echo "<script>console.log($json)</script>";
  }
}

/**
 * Replaces _ with space and makes first character capital
 */
function format_Label(string $str):string
{
  return ucwords(str_replace("_", " ", $str));
}


function logEvent(ILogger $logger, $event = "")
{
  // You will need to create a meaningful event string. This would
  // usually be the data and time, the page that was requested, the
  // user's browser, whether or not the request was successful.
  if(empty($event))
  {
    
    $dateFormatted = (new \DateTime('America/Winnipeg'))->format('Y-m-d H:i:s');
    $requestMethod = $_SERVER['REQUEST_METHOD'];
    if($requestMethod === "HEAD") return;
    $urlPath = empty($_SERVER['QUERY_STRING']) ? "index.php" :$_SERVER['QUERY_STRING'];
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    $respCode = http_response_code();

    $event = "{$dateFormatted} | {$requestMethod} | {$urlPath} | {$respCode} | {$userAgent} ";//default event
  }
  // invoke the $logger's write method to save the event in the log.
  // Depending on the type of $logger passed int, the event will
  // be saved in a database, or written to file
  $resp =  $logger->write($event);
  //var_dump($resp);
}


function getFileLogDataArr($file)
{
  if(file_exists($file)) {
    $log = file_get_contents($file);
    $logArr = explode("\n",$log);
    return $logArr;

  }else{
    return -1;
  }
}


/**
 * getCsrfToken - Returns csrf token from session
 *
 * @return void
 */
function getCsrfToken()
{
  return $_SESSION['CsrfToken'];
}

/**
 * validateCsrfToken - Validate csrf token in passed in a form
 *
 * @return bool
 */
function validateCsrfToken($csrf_value):bool
{

  if($csrf_value !== getCsrfToken())
  {
    return false;
  }
  else{
    return true;
  }
}

/**
 * moveUploadedImageFile - Moves uploaded image file to images folder
 *
 * @param  mixed $field
 * @param  mixed $filename
 * @return string
 */
function moveUploadedImageFile($field,$filename):string
{
  if(!empty($_FILES[$field]['name'])) 
  {
    //Get temporary file location
    $tmp_name = $_FILES[$field]['tmp_name'];

    
    $new_file_name = str_replace(" ","-",$filename)."-pic.jpg";
    $save_path = __DIR__ . '/../public/images/' . $new_file_name;

    // if(file_exists($save_path))
    // {
    //   $randomStr = md5(uniqid(mt_rand(),true));
    //   $new_file_name = str_replace(" ","-",$filename).substr(md5(uniqid(mt_rand(),true)),0,5)."-pic.jpg";
    //   $save_path = __DIR__ . '/../public/images/' . $new_file_name;
    // }

    if(move_uploaded_file($tmp_name, $save_path)){
      return $new_file_name;
    }

  }
}

/**
 * validateFormFields -  Validates post form fields based on required array
 *
 * @param  mixed $errors
 * @return $errors array
 */
function validateFormFields(array $errors = [])
{
  // Trim Field Values and Validate register fields
  // Trim spaces from around all values

  foreach($_POST as $key => $value) 
  {
      $_POST[$key] = trim($value);
  }

  // The following fields are required
  $required_arr = ['first_name', 'last_name', 'email', 'city', 'phone', 'street',
  'country', 'postal_code', 'province', 'password', 'confirm_password'];

  // Validate required fields from required array
  foreach($required_arr as $post_key) {
      if(empty($_POST[$post_key])) {
          $label = format_Label($post_key);
          $errors[$post_key][] = $label . " is required";
      }
  }

  // Validate first name field
  if(!preg_match('/^[A-z\s\-\,\']{1,40}$/', $_POST['first_name'])) {
      $errors['first_name'][] = strlen($_POST['first_name']) > 40 ? "First name must not be more than 40 characters": 
      'First name contains invalid characters';
  }

  // Validate last name field
  if(!preg_match('/^[\s\-\,\'A-z]{1,40}$/', $_POST['last_name'])) {
      $errors['last_name'][] = strlen($_POST['last_name']) > 40 ? "Last name must not be more than 40 characters":
      'Last name contains invalid characters';
  }

  // Validate phone number field
  if(!preg_match('/^[\d+]{10,14}$/', $_POST['phone'])) {
  $errors['phone'][] = strlen($_POST['phone']) > 14 ? "Phone number must be between 10 and 14 digits only":
      'Phone number contains invalid characters. Digits only.';
  }

  // Validate email field
  if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      $errors['email'][] =  strlen($_POST['email'])> 100 ?'Email can not be more than 100 characters':
      'Email field must be a valid email';
  }

  // Validate street field
  if(!preg_match('/^[A-z0-9\s\-\,\']{1,200}$/', $_POST['street'])) {
  $errors['street'][] = strlen($_POST['street'])> 200 ?'Street address can not be more than 200 characters':
      'Street address contains invalid characters';
  }

  // Validate city field
  if(!preg_match('/^[A-z0-9\s\-\,\']{1,50}$/', $_POST['city'])) {
  $errors['city'][] = strlen($_POST['city'])> 50 ?'City can not be more than 50 characters':
      'City contains invalid characters';
  }

  // Validate province field
  if(!preg_match('/^[A-z\s]{1,100}$/', $_POST['province'])) {
  $errors['province'][] = strlen($_POST['province'])> 100 ?'Province can not be more than 100 characters':
      'Province contains invalid characters';
  }

  // Validate country field
  if(!preg_match('/^[A-z\s]{1,100}$/', $_POST['country'])) {
  $errors['country'][] = strlen($_POST['country'])> 100 ?'Country can not be more than 100 characters':
      'Country contains invalid characters';
  }

  // Validate postal code field
  if(!preg_match('/^[A-z][0-9][A-z](\s?)[0-9][A-z][0-9]$/', $_POST['postal_code'])) {
  $errors['postal_code'][] = strlen($_POST['postal_code']) > 8 ?'Postal code can not be more than 8 characters':
      'Postal code contains invalid characters. Match format 1A23B3';
  }

  // Validate password field
  if(!preg_match('/^(?=.*[A-z])(?=.*[\d])(?=.*[[:punct:]]).{8,}$/', $_POST['password'])) {
  $errors['password'][] = strlen($_POST['password']) < 8 ? "Password can't be less than 8 characters":
      'Password must have at least one number,letter and special character. (Min 8 characters)';
  }

  //Validate confirm password field
  if($_POST['password'] != $_POST['confirm_password']){
    $errors['confirm_password'][] = 'Passwords do not match';
  }

  return $errors;
}


/**
 * addFormValuesToSession -  Adds register form fields to session
 *
 * @param  mixed $hashed_password
 * @return void
 */
function addFormValuesToSession($hashed_password, $formValues)
{
  $formValuesArr = [];
  foreach($formValues as $key => $value) 
  {
    if(!empty($formValues[$key]))
    {
      if($key === 'password'){
        $value =  $hashed_password;
      }
      
      $formValuesArr[$key] = $value;
    }
  }
  return json_encode($formValuesArr);
  
}
