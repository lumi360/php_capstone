<?php

require __DIR__ . '/connect.php';
require __DIR__ . '/escape.php';
require __DIR__ . '/functions.php';
//required files for logger
require __DIR__ . '/../app/Lib/Interfaces/ILogger.php';
require __DIR__ . '/../app/Lib/Classes/FileLogger.php';
require __DIR__ . '/../app/Lib/Classes/DatabaseLogger.php';
require __DIR__ . '/../app/Lib/Classes/Validator.php';
require_once __DIR__ . '/../app/Models/Model.php';
require_once __DIR__ . '/../app/Models/User.php';
require_once __DIR__ . '/../app/Models/DbLogger.php';
require_once __DIR__ . '/../app/Models/AdminLog.php';
require_once __DIR__ . '/../app/Models/Dashboard.php';
require_once __DIR__ . '/../app/Models/Clothing.php';
require_once __DIR__ . '/../app/Models/Order.php';
require_once __DIR__ . '/../app/Models/userprofile.php';
require_once __DIR__ . '/../app/Models/AdminLog.php';